const lang = {
    en: {
        Work: "Find Worker",
        Tranport: "Book Cab",
        Hotel: "Book Hotel",
        Kirana: "Buy Kirana",
        FruitVegitable: "Buy Fruit Vegitable",
        off:'Off',
        myOrder:'My Order'
    },
    hindi: {
        Work: "मज़दूर",
        Tranport: "टैक्सी",
        Hotel: "होटल",
        Kirana: "किराना सामान",
        FruitVegitable: "फल सब्जी",
        off:'छूट',
        myOrder:'मेरा ऑर्डर'
       
    }
}

export default lang;
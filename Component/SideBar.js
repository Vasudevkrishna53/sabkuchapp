
import React, { Component } from "react";
import { connect } from 'react-redux';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions, AsyncStorage, Picker } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";
import language from '../cms/lang/DefaultScreenLanguage';
import { lang } from "moment";
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
class SideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        }
    }
    async componentDidMount() {

        let user = await AsyncStorage.getItem("userLocalStorage", null)

        if (user) {
            user = JSON.parse(user);
        }
        this.setState({ user })

    }
    render() {
        const {
            user
        } = this.state;
        const {
            imageUrl,
            name
        } = user;
        // console.log('side bar url is-------- ,',user)

        if (user) {
            return (
                <View style={styles.body}>

                    <View style={{borderBottomWidth:1,borderColor:'#e4e0e0'}}>
                        <View style={{ padding: 10 }}>
                            {imageUrl && imageUrl.length && <Image style={{ width: 65, height: 65, borderRadius: 32.5, borderWidth: 1, borderColor: 'grey' }}
                                source={{ uri: `${imageUrl}` }}
                            />}
                            <Text style={{ fontSize: 16 }}>{name}</Text>
                        </View>
                    </View>
                    
                    <TouchableOpacity
                        style={[styles.list]}
                        onPress={() => { this.props.navigation.navigate('Kirana') }}
                    >
                        <Text style={{width:'50%', fontSize: 14, fontWeight: 'bold' }}>{language.hindi.Kirana}</Text>
                        <Image style={{ width: 30, height: 30, borderRadius: 15 }} source={require('../cms/icons/Default/Kirana.jpg')}></Image>

                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.list}
                        onPress={() => { this.props.navigation.navigate('VegitableFruit') }}
                    >
                        <Text style={{width:'50%', fontSize: 14, fontWeight: 'bold' }}>{language.hindi.FruitVegitable}</Text>
                        <Image style={{ width: 30, height: 30, borderRadius: 15 }} source={require('../cms/icons/Default/VegFruit.jpg')}></Image>

                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.list}
                        onPress={() => { this.props.navigation.navigate('Work') }}
                    >
                        <Text style={{width:'50%', fontSize: 14, fontWeight: 'bold' }} >{language.hindi.Work}</Text>
                        <Image style={{ width: 30, height: 30, borderRadius: 15 }} source={require('../cms/icons/Default/Worker.png')}></Image>

                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.list}
                        onPress={() => { this.props.navigation.navigate('Hotle') }}
                    >
                        <Text style={{width:'50%', fontSize: 14, fontWeight: 'bold' }}>{language.hindi.Hotel}</Text>
                        <Image style={{ width: 30, height: 30, borderRadius: 15 }} source={require('../cms/icons/Default/Hotel.jpg')}></Image>

                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        style={styles.list}
                        onPress={() => { this.props.navigation.navigate('Transport') }}
                    >
                        <Text style={{width:'50%', fontSize: 14, fontWeight: 'bold' }}>{language.hindi.Tranport}</Text>
                        <Image style={{ width: 30, height: 30, borderRadius: 15 }} source={require('../cms/icons/Default/Transport.jpg')}></Image>

                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.list}
                        onPress={() => { this.props.navigation.navigate('MyOrder') }}
                    >
                        <Text style={{ width:'50%',fontSize: 14, fontWeight: 'bold' }}>{language.hindi.myOrder}</Text>
                        <Image style={{ width: 30, height: 30, borderRadius: 15 }} source={require('../cms/icons/Default/MyOrder.png')}></Image>

                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('User') }}
                    >
                        <Text style={styles.list}>Log Out</Text>
                    </TouchableOpacity>

                </View>


            )
        }
        else {
            return (<View><Text>Welcome</Text></View>)
        }
    }
}

const mapStatetoProps = state => {
    // console.log("value in state is ------",state)
    return {
        user: state.user
    }
}



export default connect(mapStatetoProps, null)(SideBar);

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
        width: screenWidth * 0.55,
        height: screenHeight,
        position: 'absolute',
        // top: '7%',
        left: 0,
        zIndex: 5,
        borderRightWidth: 3,
        borderRightColor: '#e4e0e0'
    },
    list: {
        flexDirection: 'row',
        backgroundColor: 'white',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#e4e0e0',
        

    }
})


import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import { TouchableWithoutFeedback, TextInput } from "react-native-gesture-handler";
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen')

export default class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.body}>
                <View>
                    <Text>1</Text>
                    <Text>Home</Text>
                </View>
                <View>
                    <Text>1</Text>
                    <Text>Offers</Text>
                </View>
                <View>
                    <Text>1</Text>
                    <Text>Search</Text>
                </View>
                <View>
                    <Text>1</Text>
                    <Text>Category</Text>
                </View>
                <View>
                    <Text>1</Text>
                    <Text>Sort</Text>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        width: screenWidth,
        height: 55,
        position: 'absolute',
        bottom: 0,
        zIndex: 5,
        borderWidth: 1,
        borderBottomColor: '#E4E0E0',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:5

    }
})

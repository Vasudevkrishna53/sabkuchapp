
import React, { Component } from "react";
import { View, Text, Image,StyleSheet ,TouchableOpacity} from "react-native";
import { TouchableWithoutFeedback, TextInput } from "react-native-gesture-handler";

export default class Header extends Component {
    constructor(props) {
        super(props);
    }
    handelMenuBar =()=>{
        const {
            handelMenuDisplayer
        }=this.props;
        handelMenuDisplayer();

    }
    render() {
        return (
            <View style={styles.body}>
                <View style={{marginHorizontal:5}}>
                    <TouchableOpacity onPress={() => this.handelMenuBar()}>
                        <Image 
                        style={{marginVertical:5,marginRight:50,width: 35, height: 29,borderColor:'white',tintColor:'white' }} 
                        source={{ uri: 'https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/SidebarNavigationIcon.png' }} />
                    </TouchableOpacity>
                    {/* <Text>Mr Vasudev krishna</Text> */}
                </View>

                <TouchableWithoutFeedback style={{flex:1,width:500,height:'100%',flexDirection:'row'}} onPress={() => { this.handelMenuBar() }}>
                    <View>
                        <TextInput 
                        style={{ backgroundColor:'white',width:230,marginVertical:5}}
                        placeholder='Search'
                        ></TextInput>
                    </View>
                    {/* <View style={{marginLeft:50}}>
                    <Image style={{ marginVertical:5,width:35,height:35,tintColor:'white' }} 
                    source={{uri:'https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/icons/cartWhite.png'}}
                    ></Image>
                    </View> */}
                </TouchableWithoutFeedback>
           
            </View>
        )
    }
}

const styles = StyleSheet.create({
    body: {
        flexDirection:'row',
        backgroundColor: '#09099B',
        width: '100%',
        height: 55,
        position: 'absolute',
        top: 0,
        // left: -15,
        zIndex: 5,
        borderBottomWidth:5,
        borderBottomColor:'#E4E0E0',
        // flex:1
        
        
        
    },
    displayNone: {
        display: 'none'
    }
})

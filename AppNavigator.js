import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    AsyncStorage

} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import User from './Screens/User';
import Work from './Screens/Work/Work';
import Default from './Screens/Default';
import UnPickedWork from './Screens/UnPickedWork';
import WorkOwnerDetail from './Screens/WorkOwnerDetail';
import IndividualWorkList from './Screens/IndividualWorkList';
import Transport from './Screens/Transport/Transport';
import Realstate from './Screens/Realstate/Realstate';
import Flash from './Screens/Flash';
import BookingConfirmation from './Screens/BookingConfirmation';
import OtpVerification from './Screens/OtpVerification';
import SideBar from './Component/SideBar';
import Shopping from './Screens/Shopping/Shopping';
import Hotle from './Screens/Hotel/Hotle';
import VegitableFruit from './Screens/Vegitable/VegitableFruit';
import ContactUs from './Screens/ContactUs/ContactUs';
import Kirana from './Screens/Kirana/Kirana';
import MyOrder from './Screens/MyOrder';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Login from './Screens/Login/Login';

//  const initialScreen=AsyncStorage.getItem('isUserRegeistered')==1?'Default':'User'


const AppNavigator = createStackNavigator({
    Default: {
        screen: Default,
        navigationOptions: {
            headerShown: false,
        }
    },
    UnPickedWork: {
        screen: UnPickedWork,
        navigationOptions: {
            headerShown: false,
        }
    },
    User: {
        screen: User,
        navigationOptions: {
            headerShown: false,
        }
    },
    
    ContactUs: {
        screen: ContactUs,
        navigationOptions: {
            headerShown: false,
        }
    },
    
    
    Login: {
        screen: Login,
        navigationOptions: {
            headerShown: false,
        }
    },
    Work: {
        screen: Work,
        navigationOptions: {
            headerShown: false,
        }
    },
    VegitableFruit: {
        screen: VegitableFruit,
        navigationOptions: {
            headerShown: false,
        }
    },
    Hotle: {
        screen: Hotle,
        navigationOptions: {
            headerShown: false,
        }
    },
    Realstate: {
        screen: Realstate,
        navigationOptions: {
            headerShown: false,
        }
    },
    Transport: {
        screen: Transport,
        navigationOptions: {
            headerShown: false,
        }
    },
    OtpVerification:{
        screen: OtpVerification,
        navigationOptions: {
            headerShown: false,
        }
    },
    Flash:{
        screen: Flash,
        navigationOptions: {
            headerShown: false,
        }
    },
    BookingConfirmation: {
        screen: BookingConfirmation,
        navigationOptions: {
            headerShown: false,
        }
    },
    MyOrder: {
        screen: MyOrder,
        navigationOptions: {
            headerShown: false,
        }
    },

    Kirana: {
        screen: Kirana,
        navigationOptions: {
            headerShown: false,
        }
    },

    Shopping: {
        screen: Shopping,
        navigationOptions: {
            headerShown: false,
        }
    }
}
    ,
    {
        
        initialRouteName: "Flash",
    }
)

const Container = createAppContainer(AppNavigator);

export default Container;


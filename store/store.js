import promise from 'redux-promise';
import { createStore, applyMiddleware, compose } from 'redux';
import kaamKaajReducer from './reducer/kaamKaajReducer';

const store = createStore(
    kaamKaajReducer,
    compose(
        applyMiddleware(promise)
    )
);


export default store;
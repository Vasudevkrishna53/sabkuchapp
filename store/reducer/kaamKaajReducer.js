 import { Actions } from "../actions/actionConstants";


const initialState = {
    
    default: {},
    test: {},
    user:{},
    apps:[]
}

const kaamkaajReducer = (state = initialState, action) => {

    switch (action.type) {
        case Actions.saveUser : return{
            ...state,
            user:action.user
        }

        case Actions.saveApps : return{
            ...state,
            apps:action.apps
        }

        default:   
            return state;
    }



}

export default kaamkaajReducer;
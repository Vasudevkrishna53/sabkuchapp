import {Actions} from '../actions/actionConstants'


export function saveUserData(data){
    return{
        type:Actions.saveUser,
        user:data
    }
}

export function saveAppsData(data){
    return{
        type:Actions.saveApps,
        apps:data
    }
}
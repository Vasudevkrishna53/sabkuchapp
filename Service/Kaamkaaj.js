import axios from 'axios';

export const getApps = async () => {
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetApps`,
        parameters
    );

    return res.data;
}

export const getUnpickedWork = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetUnpickedWork`,
        parameters
    );
    return res.data;
}


export const getWorksMaster = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetWorkMaster`,
        parameters
    );
    return res.data;
}

export const getGetShoppingItems = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetShoppingItems`,
        parameters
    );
    return res.data;
}

export const getGetHotelList = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetHotelList`,
        parameters
    );
    return res.data;
}


export const GetVegitableFruitList = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetVegitableFruitList`,
        parameters
    );
    return res.data;
}




export const getOwnerWorks = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetOwnerWork?id=288179d0-d2b8-4d6d-9b2a-d8e1bd8d6860`,
        parameters
    );
    return res.data;
}


export const createUpdateUser = async (user) => {

    const body = user;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateUpdateUser`,
        body,
        parameters
    );
    return res.data;
}

export const CreateUpdateVegitableFruitOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateUpdateVegitableFruitOrder`,
        body,
        parameters
    );
    return res.data;
}



export const createUpdateWork = async (userWorks) => {


    let body = {
        user: userWorks.user,
        works: userWorks.selectedWork
    };
    body = JSON.stringify(body);

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateWork`,
        body,
        parameters
    );
    return res.data;
}



export const getNewOrderID = async (request) => {


    let body = {
        amount: request.amount,
        receipt: request.receipt
    };
    body = JSON.stringify(body);

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetRazorPayOrderID`,
        body,
        parameters
    );
    return res.data;
}

export const verifyMobileNo = async (request) => {


    let body = {
        mobileNo: request.mobileNumber,
        mobileNoOtp: request.mobileNoOtp
    };
    body = JSON.stringify(body);

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    } 

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/VerifyMobileNo`,
        body,
        parameters
    );
    return res.data;
}


export const GetMyOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetMyOrder`,
        body,
        parameters
    );
    return res.data;
}

export const DeleteMyOrders = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/DeleteMyOrders`,
        body,
        parameters
    );
    return res.data;
}


export const UpdatePaymentOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/UpdatePaymentOrder`,
        body,
        parameters
    );
    return res.data;
}



export const GetKiranaList = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetKiranaList`,
        parameters
    );
    return res.data;
}

export const CreateUpdateKiranaOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateUpdateKiranaOrder`,
        body,
        parameters
    );
    return res.data;
}


export const CreateUpdateTransportOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateUpdateTransportOrder`,
        body,
        parameters
    );
    return res.data;
}

export const UpdatePaymentOrderV1 =(request)=>
{

    
    return axios({
        method: 'post',
        url: `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/UpdatePaymentOrder`,
        data: request
    }).then((data) => { 
           
            return Promise.resolve(data.data) 
        }).catch((err) => { return Promise.reject(err) });

}

export const UpdateCustomerRemarks = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/UpdateCustomerRemarks`,
        body,
        parameters
    );
    return res.data;
}


export const GetWork = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetWork`,
        parameters
    );
    return res.data;
}


export const CreateUpdateWorkOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateUpdateWorkOrder`,
        body,
        parameters
    );
    
    return res.data;
}


export const GetTransport = async () => {

    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.get(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetTransport`,
        parameters
    );
    return res.data;
}


export const CreateUpdateHotelOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateUpdateHotelOrder`,
        body,
        parameters
    );
    return res.data;
}

export const CreateUpdateShoppingOrder = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/CreateUpdateShoppingOrder`,
        body,
        parameters
    );
    return res.data;
}

export const GetLoginUser = async (request) => {

    const body = request;
    const parameters = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    const res = await axios.post(
        `https://ureue3oysj.execute-api.ap-south-1.amazonaws.com/Prod/api/GetLoginUser`,
        body,
        parameters
    );
    return res.data;
}






import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid,
    Dimensions,
    AsyncStorage,
    BackHandler
} from 'react-native';

import SideBar from '../Component/SideBar';
import Header from '../Component/Header';
import RazorpayCheckout from 'react-native-razorpay';

import language from '../cms/lang/language';
import { getWorksMaster, createUpdateWork, UpdatePaymentOrderV1, UpdateCustomerRemarks } from '../Service/Kaamkaaj';
import { TouchableHighlight } from 'react-native-gesture-handler';

import { connect } from 'react-redux';
import {
    getNewOrderID
} from '../Service/Kaamkaaj';
import { isResumable } from 'react-native-fs';

const screenHeight = Math.round(Dimensions.get('screen').height)

class BookingConfirmation extends Component {
    constructor(props) {
        super(props);
        this.props;

        this.state = {
            isSideBarDisplayed: false,
            workImage: this.props.navigation.getParam('image', 'noPhote'),
            appType: this.props.navigation.getParam('appType', 'no-Work'),
            amount: this.props.navigation.getParam('price', 0),
            items: this.props.navigation.getParam('items', "item1,item2"),
            orderNo: this.props.navigation.getParam('orderNo', ""),
            showSearchingCard: false,
            showConfirmationCard: true,
            showSubmitProgressBar: false,
            isPaymentSuccess: false,
            paymentTxnNo: '',
            user: this.props.navigation.getParam('user', null),
        };
    }


    backAction = () => {
        this.props.navigation.navigate('Default')
    };

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction());
    }
    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    GetOrderNo = async () => {
        const {
            amount,
            appType
        } = this.state;
        let request = {};
        request.amount = parseInt(amount, 10) * 100   //5000;
        request.receipt = appType
        let response = await getNewOrderID(request);
        if (response) {
            return response.id
        }
    }

    openRazorpay = (appType, orderId) => {
        let {
            amount: price,

        } = this.state;
        price = (parseInt(price, 10) * 100).toString()
        this.setState({ showSubmitProgressBar: true })
        this.GetOrderNo().then((id) => {
            var options = {
                description: 'Credits towards consultation',
                image: 'https://i.imgur.com/3g7nmJC.png',
                currency: 'INR',
                key: 'rzp_test_57XjyzhNYJTrtv',
                amount: price,
                name: 'KaamKaaj',
                order_id: id,//Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
                prefill: {
                    email: 'Vasudevdjdskfjsdkf@gmail.com',
                    contact: '8851057104',
                    name: 'Vasudev Kumar'
                },
                theme: { color: '#53a20e' }
            }
            RazorpayCheckout.open(options).then((data) => {
                // handle success
                // alert(`Success: ${data.razorpay_payment_id}`);
                if (data.razorpay_payment_id) {
                    UpdatePaymentOrderV1({ AppType: appType, OrderId: orderId, PaymentMode: "Online RazorPay", PaymentDate: "", PaymentStatus: 5, PaymentTransactionNo: data.razorpay_payment_id }).then(x => {
                        this.setState({ showSubmitProgressBar: false, isPaymentSuccess: true, paymentTxnNo: data.razorpay_payment_id })

                    })
                }
                else {
                    UpdatePaymentOrderV1({ AppType: appType, OrderId: orderId, PaymentMode: "Online RazorPay", PaymentDate: "", PaymentStatus: 2, PaymentTransactionNo: "Payment Failure from Razorpay" }).then(x => {
                        this.setState({ showSubmitProgressBar: false, isPaymentSuccess: true, paymentTxnNo: data.razorpay_payment_id })

                    })
                }
                //  this.setState({ showSubmitProgressBar: false, isPaymentSuccess: true, paymentTxnNo: data.razorpay_payment_id })
            }).catch((error) => {
                // handle failure
                // alert('fail is the out ocme')
                this.setState({ showSubmitProgressBar: false })
                alert(`Error: ${error.code} | ${error.description}`);
            });

        })
    }


    UpdateCustomerExperience = async (appType, orderId, remarks) => {

        let respose = await UpdateCustomerRemarks({ AppType: appType, OrderId: orderId, CustomerRemarks: remarks })

        if (respose.hasError == false) {
            this.props.navigation.navigate('Default')

        }
    }

    renderConfirmationCard = () => {
        const {
            isSideBarDisplayed,
            workImage,
            appType,
            showSearchingCard,
            showConfirmationCard,
            showSubmitProgressBar,
            isPaymentSuccess,
            paymentTxnNo,
            amount,
            orderNo,
            items,
            user
        } = this.state;

        const {
            name,
            mobileNumber,
            address
        } = user;
        const {
            city,
            address1,
            address2,
            pincode,
        } = address
        const {
            orderno: lang_order,
            shareExp: lang_shareExp,
            deliveryMessage: lang_deliveryMessage,
            orderDone: lang_orderDone,
            items: lang_items,
            awsome: lang_awsome,
            veryGood: lang_veryGood,
            bad: lang_bad,
            average: lang_average,
            name:lang_name,
            address:lang_address,
            mobileNo:lang_mobileNumber

        } = language.hindi



        if (appType === "5") {
            return (
                <View style={{ borderRadius: 16, width: '100%', backgroundColor: 'white', marginVertical: 60 }}>
                    <View style={{ marginHorizontal: 10 }}>
                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'green' }}>{lang_orderDone}</Text>

                        </View>

                        <Text style={{ fontSize: 16 }}>{order} : {orderNo}</Text>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <Text style={{ fontSize: 16, fontWeight: '700' }}>Hotel:</Text>
                            {items && items.length > 0 && items.map(x => {
                                return (<Text>{x.itemName} , </Text>)
                            })}
                        </View>

                    </View>


                    <View style={{ marginHorizontal: 10 }}>
                        <Text style={{ marginTop: 10, color: 'green', fontSize: 16, fontWeight: 'bold' }}>{shareExp}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text
                                style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Awsome') }}
                            >Awsome</Text>
                            <Text style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Very Good') }}
                            >Very Good</Text>
                            <Text style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Average') }}
                            >Average</Text>
                            <Text style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Bad') }}
                            >Bad</Text>


                        </View>
                    </View>


                </View>
            )
        }

        
        else if(appType==="3") {
            return (
                <View style={{ borderRadius: 16, width: '100%', backgroundColor: 'white', marginVertical: 60, padding: 5 }}>
                    <View style={{ alignSelf: 'center', paddingVertical: 20, flexDirection: 'row' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'green' }}>{lang_orderDone}</Text>

                    </View>
                    <View style={{ paddingVertical: 20, flexDirection: 'row', borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>
                        <Text style={{ fontSize: 16, marginRight: 10 }}>{lang_order}</Text>
                        <Text style={{ fontSize: 16 }}>{orderNo}</Text>
                    </View>

                    <View style={{ paddingVertical: 20, flexDirection: 'row', flexWrap: 'wrap', borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>
                        <Text style={{ fontSize: 16, marginRight: 10 }}>{lang_items}</Text>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            {items && items.length > 0 && items.map(x => {
                                return (<Text>{x.itemName} , </Text>)
                            })}
                        </View>
                    </View>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>{lang_deliveryMessage}</Text>
                    <View style={{ paddingVertical: 20, borderWidth: 1, borderColor: 'black' }} >
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <Text style={{paddingHorizontal:5}}>{lang_name} - {name}</Text>
                            <Text style={{paddingHorizontal:5}}>{lang_address} -{address1}</Text>
                            <Text style={{paddingHorizontal:5}}>{address2}</Text>
                            <Text style={{paddingHorizontal:5}}>{`${city} ${pincode}`}</Text>
                            <Text style={{paddingHorizontal:5}}>{lang_mobileNumber} -{mobileNumber}</Text>

                        </View>

                    </View>



                    <View style={{ marginTop: 20, borderWidth: 1, borderColor: 'black' }}>
                        <Text style={{ marginTop: 10, color: 'green', fontSize: 16, fontWeight: 'bold' }}>{lang_shareExp}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text
                                style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Awsome') }}
                            >{lang_awsome}</Text>
                            <Text style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Very Good') }}
                            >{lang_veryGood}</Text>
                            <Text style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Average') }}
                            >{lang_average}</Text>
                            <Text style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Bad') }}
                            >{lang_bad}</Text>


                        </View>
                    </View>

                </View>
            )
        }

        else{
            return (
                <View style={{ borderRadius: 16, width: '100%', backgroundColor: 'white', marginVertical: 60, padding: 5 }}>
                    <View style={{ alignSelf: 'center', paddingVertical: 20, flexDirection: 'row' }}>
                        <Text style={{ fontSize: 24, fontWeight: 'bold', color: 'green' }}>{lang_orderDone}</Text>

                    </View>
                    <View style={{ paddingVertical: 20, flexDirection: 'row', borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>
                        <Text style={{ fontSize: 16, marginRight: 10 }}>{lang_order}</Text>
                        <Text style={{ fontSize: 16 }}>{orderNo}</Text>
                    </View>

                    <View style={{ paddingVertical: 20, flexDirection: 'row', flexWrap: 'wrap', borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>
                        <Text style={{ fontSize: 16, marginRight: 10 }}>{lang_items}</Text>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            {items && items.length > 0 && items.map(x => {
                                return (<Text>{x.itemName} , </Text>)
                            })}
                        </View>
                    </View>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>{lang_deliveryMessage}</Text>
                    <View style={{ paddingVertical: 20, borderWidth: 1, borderColor: 'black' }} >
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <Text style={{paddingHorizontal:5}}>{lang_name} - {name}</Text>
                            <Text style={{paddingHorizontal:5}}>{lang_address} -{address1}</Text>
                            <Text style={{paddingHorizontal:5}}>{address2}</Text>
                            <Text style={{paddingHorizontal:5}}>{`${city} ${pincode}`}</Text>
                            <Text style={{paddingHorizontal:5}}>{lang_mobileNumber} -{mobileNumber}</Text>

                        </View>

                    </View>



                    <View style={{ marginTop: 20,marginBottom: 20, borderWidth: 1, borderColor: 'black' }}>
                        <Text style={{ marginTop: 10, color: 'green', fontSize: 16, fontWeight: 'bold' }}>{lang_shareExp}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text
                                style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Awsome') }}
                            >{lang_awsome}</Text>
                            <Text style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Very Good') }}
                            >{lang_veryGood}</Text>
                            <Text style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Average') }}
                            >{lang_average}</Text>
                            <Text style={{ fontSize: 16, color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Bad') }}
                            >{lang_bad}</Text>


                        </View>
                    </View>

                </View>
            )
        }


    }

    render() {
        const {
            isSideBarDisplayed,
            workImage,
            appType,
            showSearchingCard,
            showConfirmationCard,
            showSubmitProgressBar,
            isPaymentSuccess,
            paymentTxnNo,
            amount,
            orderNo,
            items,
            user
        } = this.state;

        const {
            name,
            mobileNumber,
            address
        } = user;
        const {
            city,
            address1,
            address2,
            pincode,
        } = address



        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <ScrollView style={{ height: screenHeight, backgroundColor: 'pink' }}>
                    <View style={{ marginHorizontal: 10 }}>

                        <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                            <View>


                                {
                                    showConfirmationCard && isPaymentSuccess == false && this.renderConfirmationCard()

                                    //    <View style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 16, width: '100%', backgroundColor: 'white', marginVertical: 60 }}>
                                    //         <View style={{ marginHorizontal: 10 }}>
                                    //             <Text style={{ fontSize: 24, fontWeight: 'bold', color: 'green' }}>Order Confirmed</Text>
                                    //             <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Order no : {orderNo}</Text>
                                    //             <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                    //                 <Text style={{ fontSize: 18, fontWeight: '700' }}>Items:</Text>
                                    //                 {items && items.length > 0 && items.map(x => {
                                    //                     return (<Text>{x.itemName} , </Text>)
                                    //                 })}
                                    //             </View>
                                    //             <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Our Deleivery executive will arrive soon at below address:</Text>
                                    //             <View >
                                    //                 <View>
                                    //                     <Text>{name}</Text>
                                    //                     <Text>{address1}</Text>
                                    //                     <Text>{address2}</Text>
                                    //                     <Text>{`${pincode} ${city}`}</Text>
                                    //                     <Text>{mobileNumber}</Text>

                                    //                 </View>

                                    //             </View>

                                    //         </View>
                                    //         <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    //             <Text style={{ marginTop: 10, color: 'green', fontSize: 18, fontWeight: 'bold' }}>Pay now and save Rs 10</Text>
                                    //             <TouchableOpacity
                                    //                 onPress={() => {
                                    //                     this.openRazorpay(appType, orderNo);

                                    //                 }}

                                    //                 style={{ flexDirection: 'row', marginHorizontal: 10, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', height: 40, width: 100, borderRadius: 10 }}>
                                    //                 <Text style={{ color: 'white', fontSize: 20 }}>{`\u20B9 ${amount} PAY`}</Text>
                                    //                 <ProgressBarAndroid style={{ display: `${showSubmitProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                                    //             </TouchableOpacity>
                                    //         </View>
                                    //     </View>



                                }
                                {
                                    isPaymentSuccess && <View style={{ borderRadius: 16, width: '100%', height: 200, backgroundColor: 'white', marginVertical: 60 }}>
                                        <View style={{ marginHorizontal: 10 }}>
                                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'green' }}>Payment Successfull</Text>
                                            <Text style={{ fontSize: 16 }}>Order no : {orderNo}</Text>
                                            <Text style={{ fontSize: 16 }}>TransactionID : {paymentTxnNo}</Text>
                                        </View>

                                        <View style={{ marginHorizontal: 10 }}>
                                            <Text style={{ marginTop: 10, color: 'green', fontSize: 16, fontWeight: 'bold' }}>Share your Experiance</Text>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text
                                                    style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                                    onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Awsome') }}
                                                >Awsome</Text>
                                                <Text style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                                    onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Very Good') }}
                                                >Very Good</Text>
                                                <Text style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                                    onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Average') }}
                                                >Average</Text>
                                                <Text style={{ fontSize: 16, textDecorationLine: 'underline' }}
                                                    onPress={() => { this.UpdateCustomerExperience(appType, orderNo, 'Bad') }}
                                                >Bad</Text>


                                            </View>
                                        </View>

                                    </View>
                                }
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </ScrollView>
            </SafeAreaView>

        )
    }

}

const mapStatetoProps = state => {
    return {
        state
    }
}

const mapDispatchtoProps = dispatch => {
    return {
        saveUserData: data => dispatch(saveUserData(data))
    };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(BookingConfirmation);
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid,
    Dimensions,
    AsyncStorage
} from 'react-native';

import SideBar from '../../Component/SideBar';

import Header from '../../Component/Header';

import language from '../../cms/lang/language';

import { getGetHotelList, CreateUpdateHotelOrder } from '../../Service/Kaamkaaj';

import { TouchableHighlight } from 'react-native-gesture-handler';

import { SliderBox } from "react-native-image-slider-box";
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen')



export default class Hotel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showWorkModal: false,
            shoppingList: [],
            workMaster: [],
            isSideBarDisplayed: false,
            user: {
                id: "288179d0-d2b8-4d6d-9b2a-d8e1bd8d6860"
            },
            selectedItem: [],
            showCurrentAddress: true,
            showEditAddress: false,
            selectedValue: 1,
            showItemNumber: false,
            num: [],
            showSearchingCard: false,
            showBookedWorkerCard: false,
            createUpdateHotelrequest: {
                userHotelOrders: {
                    OrderId: null,
                    TotalPrice: "900",
                    UserId: "12345",
                    IsActive: "1",
                    CreatedOn: null,
                    UpdatedOn: null,
                    PaymentStatus: "1",
                    DeliveryStatus: "2",
                    CustomerRemarks: "",
                    AgentRemarks: "",
                    Address: "fkjds",
                    UserMobileNo: "kkkk",
                    Items: "jhbjkjnkj"
                },
                hotelOrderItems: [
                    {
                        Id: null,
                        OrderNo: "fsdfsd",
                        MobileNo: "45654",
                        UserId: "456754",
                        ItemId: "r456765",
                        ItemName: "45676",
                        ItemUnit: "4567",
                        ItemPrice: "4567",
                        ItemQuantity: "9876",
                        IsActive: "1",
                        CreatedOn: "djlf",
                        Zorder: "2345"
                    }
                ]
            },
            loadingArray: [1, 2, 3, 4, 5, 6, 7],
            userHotelList: [],

        }
    }

    async componentDidMount() {
        const {
            num
        } = this.state;
        const result = await getGetHotelList();


        if (result && result.length > 0) {
            result.sort(function (x, y) {
                if (x.zOrder < y.zOrder) {
                    return -1;
                }
                else {
                    return 1;
                }
            })
            this.setState({
                shoppingList: result
            })
        }

    }





    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }




    orderNow = async (obj) => {
        const {
            userHotelList,
            userTotalPrice,
            showSubmitProgressBar,
            createUpdateHotelrequest
        } = this.state;
        this.setState({ showSubmitProgressBar: true })



        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);

            userHotelList.push({ id: 0, orderNo: 0, mobileNo: user.mobileNumber, userId: user.id, itemId: obj.id, itemName: obj.name, itemUnit: 'Room', itemPrice: 1 * obj.price, itemQuantity:1, isActive: 1, zorder: 1 })


            let itemsList = "";
            userHotelList.map(x => {
                itemsList += `${x.itemName} ${x.itemQuantity} ${x.itemUnit} \u20B9${x.itemPrice} ,`
            })
            createUpdateHotelrequest.hotelOrderItems = userHotelList;
            createUpdateHotelrequest.userHotelOrders =
                { OrderId: 0, TotalPrice: obj.price, UserId: user.id, IsActive: 1, PaymentStatus: 1, DeliveryStatus: 1, CustomerRemarks: "", AgentRemarks: "", Address: `${user.address.address1} ${user.address.address2} ${user.address.city} ${user.address.pincode} `, UserMobileNo: user.mobileNumber, Items: itemsList }

        }

        
        let response = await CreateUpdateHotelOrder(createUpdateHotelrequest);
        if (response) {

            this.setState({ showSubmitProgressBar: false })
            let user = await AsyncStorage.getItem("userLocalStorage", null)

            if (user) {
                user = JSON.parse(user);
            }
            this.props.navigation.navigate('BookingConfirmation', { image: "image0", appType: "5", price: response.userHotelOrders.userTotalPrice, items: response.hotelOrderItems, orderNo: response.userHotelOrders.orderId, user })
        }
        else {
            this.setState({ showSubmitProgressBar: false })
        }


    }



    render() {
        const {

            shoppingList,
            isSideBarDisplayed,
            showItemNumber,
            num,
            showSearchingCard,
            showBookedWorkerCard,
            loadingArray
        } = this.state;

        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <ScrollView style={styles.body}>


                    <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                        <View style={{ flex: 1 }}>

                            <View style={{ backgroundColor: '#84C2F9', flex: 1, flexDirection: 'column' }}>

                                <View style={{ marginTop: 10, marginHorizontal: 5 }}>
                                    {
                                        shoppingList && shoppingList.length > 0 ? shoppingList.map((x, index) => {
                                            const {
                                                name,
                                                id,
                                                price,
                                                quantity,
                                                photo,
                                                photo1,
                                                photo2,
                                                description,
                                                description1,
                                                description2,
                                                description3,
                                                description4,
                                                description5,
                                                description6,
                                                description7,
                                                unit
                                            } = x;
                                            return (
                                                <View
                                                    style={[styles.chips]}
                                                >
                                                    <View style={{ width: screenWidth / 2 }}>
                                                        
                                                        <Text style={{ color: 'black', fontSize: 16, fontWeight: 'bold' }}>
                                                            {/* {language.hindi.workMaster.filter(x => x.id == id)[0].value} */}
                                                            {name} HOTEL
                                                        </Text>
                                                        <Text style={{ color: 'black', fontSize: 12 }}>
                                                            {`\u2605 \u20B9 ${price}/${unit}`}
                                                        </Text>
                                                    
                                                        <Text style={{ fontSize: 12 }}>
                                                            {`\u2605 ${description}`}
                                                        </Text>
                                                        <Text style={{ fontSize: 12 }}>
                                                            {`\u2605 ${description1}`}
                                                        </Text>
                                                        <Text style={{ fontSize: 12 }}>
                                                            {`\u2605 ${description2}`}
                                                        </Text>
                                                        <Text style={{ fontSize: 12 }}>
                                                            {`\u2605 ${description3}`}
                                                        </Text>
                                                        <Text style={{ fontSize: 12 }}>
                                                            {`\u2605 ${description4}`}
                                                        </Text>
                                                        <Text style={{ fontSize: 12 }}>
                                                            {`\u2605 ${description5}`}
                                                        </Text>
                                                        <Text style={{ fontSize: 12 }}>
                                                            {`\u2605 ${description6}`}
                                                        </Text>

                                                        <TouchableOpacity
                                                            onPress={() => {
                                                                this.orderNow(x)
                                                            }}
                                                            style={{ padding: 5, marginVertical: 10, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: 170, height: 40, backgroundColor: '#0A1AF5', fontWeight: 'bold', borderRadius: 8 }}>
                                                            <Text style={{ color: 'white', fontSize: 16 }}>BOOK NOW</Text>
                                                        </TouchableOpacity>

                                                    </View>
                                                    <View style={{ marginHorizontal: 5 }}>

                                                        <SliderBox
                                                            images={[photo, photo1, photo2]}
                                                            onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                                                            sliderBoxHeight={195}
                                                            parentWidth={195}
                                                            dotColor="#FFEE58"
                                                            inactiveDotColor="white"
                                                            autoplay
                                                            dotStyle={{
                                                                width: 15,
                                                                height: 15,
                                                                borderRadius: 15,
                                                                marginHorizontal: 10,
                                                                padding: 0,
                                                                margin: 0
                                                            }}

                                                        />
                                                    </View>
                                                </View>
                                            )
                                        }) : loadingArray.map(x => {
                                            return (
                                                <View style={{
                                                    backgroundColor: 'white',
                                                    width: screenWidth,
                                                    height: 90,
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',
                                                    borderRadius: 8,
                                                    marginVertical: 5



                                                }}>

                                                    <View
                                                        style={{
                                                            height: 80, width: 150,
                                                            backgroundColor: '#d3d3d3', margin: 5,

                                                        }}
                                                    >

                                                    </View>

                                                    <View style={{ justifyContent: 'space-around' }} >
                                                        <View
                                                            style={{
                                                                height: 15, width: 100,

                                                                backgroundColor: '#d3d3d3'
                                                            }}
                                                        >

                                                        </View>
                                                        <View
                                                            style={{ height: 15, width: 100, backgroundColor: '#d3d3d3' }}
                                                        >

                                                        </View>

                                                    </View>

                                                    <View style={{ justifyContent: 'space-around' }}>
                                                        <View
                                                            style={{ height: 15, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                                        >

                                                        </View>
                                                        <View
                                                            style={{ height: 50, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                                        >

                                                        </View>
                                                    </View>
                                                </View>
                                            )

                                        })
                                    }
                                </View>
                            </View>

                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    body: {
        marginTop: 40,

    },
    chips: {

        // borderWidth: 1,
        // borderColor: '#f0f0f0',
        borderRadius: 8,
        backgroundColor: 'white',
        // width: 195,
        // height: 268,
        marginVertical: 5,

        flexDirection: 'row'

    },
    example: {
        marginVertical: 0,
    },
    activeChips: {
        backgroundColor: 'green'
    },
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },
    card: {

        // backgroundColor: 'white',
        // borderRadius: 8,
        // height: 300,
        // marginBottom: 15,
        // overflow: 'scroll',


    },
    label: {
        fontSize: 16
    },
    textbox: {
        borderRadius: 18,
        borderWidth: 1,
        width: '100%',
        height: 50,
        fontSize: 16
    },
    submitBtn: {
        width: 164,
        height: 46,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'red',
        fontSize: 16
    }
});

import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid,
    Dimensions,
    AsyncStorage,
    BackHandler
} from 'react-native';

import SideBar from '../../Component/SideBar';

import Header from '../../Component/Header';

import language from '../../cms/lang/language';

import { GetVegitableFruitList, CreateUpdateVegitableFruitOrder } from '../../Service/Kaamkaaj';

import { TouchableHighlight } from 'react-native-gesture-handler';

import { SliderBox } from "react-native-image-slider-box";
import { connect } from 'react-redux';
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen')



class ContactUs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingArray: [1, 2, 3, 4, 5, 6],
            showWorkModal: false,
            vegitableFruitList: [],
            workMaster: [],
            isSideBarDisplayed: false,
            user: {
                id: ''
            },
            selectedItem: [],
            showCurrentAddress: true,
            showEditAddress: false,
            address1: '',
            address2: '',
            pincode: '',
            state: '',
            selectedValue: 1,
            showItemNumber: false,
            num: [],
            showSearchingCard: false,
            showBookedWorkerCard: false,
            userVegitableFruitList: [],
            userTotalPrice: 0,
            showSubmitProgressBar: false,
            createUpdateVegitableFruitrequest: {
                userVegitableFruitOrders: {
                    OrderId: null,
                    TotalPrice: "900",
                    UserId: "12345",
                    IsActive: "1",
                    CreatedOn: null,
                    UpdatedOn: null,
                    PaymentStatus: "1",
                    DeliveryStatus: "2",
                    CustomerRemarks: "",
                    AgentRemarks: "",
                    Address: "fkjds",
                    UserMobileNo: "kkkk",
                    Items: "jhbjkjnkj"
                },
                vegitableFruitOrderItems: [
                    {
                        Id: null,
                        OrderNo: "fsdfsd",
                        MobileNo: "45654",
                        UserId: "456754",
                        ItemId: "r456765",
                        ItemName: "45676",
                        ItemUnit: "4567",
                        ItemPrice: "4567",
                        ItemQuantity: "9876",
                        IsActive: "1",
                        CreatedOn: "djlf",
                        Zorder: "2345"
                    }
                ]
            }
        }
    }

    backAction = () => {
        // Alert.alert("Hold on!", "Are you sure you want to go back?", [
        //     {
        //         text: "Cancel",
        //         onPress: () => null,
        //         style: "cancel"
        //     },
        //     { text: "YES", onPress: () => BackHandler.exitApp() }
        // ]);
        // return true; 
        // this.setState({
        //     vegitableFruitList: []
        // })

        //  alert('a');

        this.props.navigation.navigate('Default')
    };

  

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction);
    }



    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    handelItemQuantity = (x) => {
        const {
            selectedItem,
            workMaster,
            vegitableFruitList
        } = this.state;
        const index = vegitableFruitList.findIndex(x => x.id === selectedItem);
        if (index !== -1) {
            const price = vegitableFruitList[index].price / vegitableFruitList[index].quantity;
            vegitableFruitList[index].price = price * x;
            vegitableFruitList[index].quantity = x
        }
        this.setState({ showItemNumber: false, vegitableFruitList })
    }

    setQuantity = (quantity, id) => {
        const {
            vegitableFruitList
        } = this.state;
        const itemIndex = vegitableFruitList.findIndex(x => x.id === id);
        if (itemIndex !== -1) {
            vegitableFruitList[itemIndex].itemQuantity = quantity;
            vegitableFruitList[itemIndex].totalPrice = vegitableFruitList[itemIndex].price * quantity

        }
        this.setState(vegitableFruitList);

    }

    addItem = async (id, itemQuantity, unit, price, name) => {

        let {
            
            userVegitableFruitList,
            userTotalPrice
        } = this.state;

        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);
        }

        if (userVegitableFruitList && userVegitableFruitList.length > 0) {
            const index = userVegitableFruitList.findIndex(x => x.itemId === id)
            if (index !== -1) {
                userVegitableFruitList[index].itemQuantity =userVegitableFruitList[index].itemQuantity+ itemQuantity;
                userVegitableFruitList[index].itemPrice = userVegitableFruitList[index].itemPrice+(itemQuantity * price);

            }
            else {
                userVegitableFruitList.push({ id: 0, orderNo: 0, mobileNo: user.mobileNumber, userId: user.id, itemId: id, itemName: name, itemUnit: unit, itemPrice: itemQuantity * price, itemQuantity, isActive: 1, zorder: 1 })

            }
        }
        else {
            userVegitableFruitList.push({ id: 0, orderNo: 0, mobileNo: user.mobileNumber, userId: user.id, itemId: id, itemName: name, itemUnit: unit, itemPrice: itemQuantity * price, itemQuantity, isActive: 1, zorder: 1})
        }


        userTotalPrice += itemQuantity * price;
        this.setState({ userVegitableFruitList, userTotalPrice });

    }
    removeSelectedItem = (id, quantity, totalPrice) => {
        let {
            userTotalPrice,
            userVegitableFruitList
        } = this.state;
        const index = userVegitableFruitList.findIndex(x => x.itemId === id)
        if (index !== -1) {
            userTotalPrice -= totalPrice;
            userVegitableFruitList.splice(index, 1)
        }
        this.setState({ userVegitableFruitList, userTotalPrice })

    }

    orderNow = async () => {
        const {
            userVegitableFruitList,
            userTotalPrice,
            showSubmitProgressBar,
            createUpdateVegitableFruitrequest
        } = this.state;
        this.setState({ showSubmitProgressBar: true })

        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);

            let itemsList = "";
            userVegitableFruitList.map(x => {
                itemsList += `${x.itemName} ${x.itemQuantity} ${x.itemUnit} \u20B9${x.itemPrice} ,`
            })
            createUpdateVegitableFruitrequest.vegitableFruitOrderItems = userVegitableFruitList

            createUpdateVegitableFruitrequest.userVegitableFruitOrders =
            { OrderId: 0, TotalPrice: userTotalPrice, UserId: user.id, IsActive: 1, PaymentStatus: 1, DeliveryStatus: 1, CustomerRemarks: "", AgentRemarks: "", Address: `${user.address.address1} ${user.address.address2} ${user.address.city} ${user.address.pincode} `, UserMobileNo: user.mobileNumber, Items: itemsList }

        }

    

        let response = await CreateUpdateVegitableFruitOrder(createUpdateVegitableFruitrequest);
        if (response) {

            this.setState({ showSubmitProgressBar: false })
            let user = await AsyncStorage.getItem("userLocalStorage", null)
            
            if (user) {
                user = JSON.parse(user);
                this.props.navigation.navigate('BookingConfirmation', { image: "image0", appType: "8", price: userTotalPrice, items: response.vegitableFruitOrderItems, orderNo: response.userVegitableFruitOrders.orderId, user })

            }
        }
        else {
            this.setState({ showSubmitProgressBar: false })
        }
    }

    render() {
        const {
            showSubmitProgressBar,
            vegitableFruitList,
            isSideBarDisplayed,
            showItemNumber,
            num,
            showSearchingCard,
            showBookedWorkerCard,
            userVegitableFruitList,
            userTotalPrice,
            loadingArray

        } = this.state;

        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                

                <ScrollView style={styles.body}>


                    <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                        <View style={{ justifyContent:'center',alignItems:'center',backgroundColor:'pink',width:screenWidth,height:screenHeight}}>
<Text>Call us : 8851057104</Text>

                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </SafeAreaView>
        )
    }
}


const mapStatetoProps = state => {
    return {
        state
    }
}

const mapDispatchtoProps = dispatch => {
    return {
        saveUserData: data => dispatch(saveUserData(data))
    };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(ContactUs);

const styles = StyleSheet.create({

    body: {
        marginTop: 40,

    },
    chips: {

        // borderWidth: 1,
        // borderColor: '#f0f0f0',
        borderRadius: 8,
        backgroundColor: 'white',
        // width: 195,
        height: 90,
        marginVertical: 5,

        flexDirection: 'row'

    },
    example: {
        marginVertical: 0,
    },
    activeChips: {
        backgroundColor: 'green'
    },
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },
    card: {

        // backgroundColor: 'white',
        // borderRadius: 8,
        // height: 300,
        // marginBottom: 15,
        // overflow: 'scroll',


    },
    label: {
        fontSize: 16
    },
    textbox: {
        borderRadius: 18,
        borderWidth: 1,
        width: '100%',
        height: 50,
        fontSize: 16
    },
    submitBtn: {
        width: 164,
        height: 46,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'red',
        fontSize: 16
    }
});

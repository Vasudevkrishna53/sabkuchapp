import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid,
    Dimensions,
    AsyncStorage,
    BackHandler,
    Alert
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import moment from "moment";

import SideBar from '../Component/SideBar';
import Header from '../Component/Header';
import RazorpayCheckout from 'react-native-razorpay';
//import SkeletonContent from 'react-native-skeleton-content';

import language from '../cms/lang/language';
import DefaultScreenlanguage from '../cms/lang/DefaultScreenLanguage';

import { GetMyOrder, DeleteMyOrders, getNewOrderID, UpdatePaymentOrder, UpdatePaymentOrderV1 } from '../Service/Kaamkaaj';
import { TouchableHighlight } from 'react-native-gesture-handler';

import { connect } from 'react-redux';


const { height: screenHeight, width: screenWidth } = Dimensions.get('screen')

class MyOrder extends Component {
    constructor(props) {
        super(props);
        this.props;

        this.state = {
            isSideBarDisplayed: false,
            vegitableFruitList: {

            },
            orders: [],
            user: {},

            isPaymentSuccess: false,
            paymentTxnNo: '',
            sort: {
                days: 0,
                apptype: "0"
            },
            unSortedOrders: [],
            loadingArray: [1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 6, 7],
            displayLoader: true,
            apiResponse: { hasError: false, message: '' },

        };
    }

    backAction = () => {
        // Alert.alert("Hold on!", "Are you sure you want to go back?", [
        //     {
        //         text: "Cancel",
        //         onPress: () => null,
        //         style: "cancel"
        //     },
        //     { text: "YES", onPress: () => BackHandler.exitApp() }
        // ]);
        // return true; 

        this.props.navigation.navigate('VegitableFruit')
    };

    async componentDidMount() {
        const {
            orders,
            apiResponse
        } = this.state;
        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
        );

        let user = await AsyncStorage.getItem("userLocalStorage", null)

        if (user) {
            user = JSON.parse(user);

            let response = await GetMyOrder({ UserMobileNo: user.mobileNumber })

            if (response && response.userOrders && response.userOrders.length > 0) {
                // response.userOrders.forEach(x => {
                //     if(x.createdOn.length>=10){
                //     x.createdOn = moment(x.createdOn.slice(0,11))
                //     }
                // })

                response.userOrders.sort((x, y) => {

                    return y.orderId - x.orderId

                })

                this.setState({ unSortedOrders: response.userOrders, orders: response.userOrders, user, displayLoader: false })
            }
            else if (response && response.hasError === true) {
                apiResponse.hasError = response.hasError
                apiResponse.message = response.message

                this.setState({ orders, apiResponse })
            }
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction);
    }

    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    getItemDeliveryStatus = (statusID) => {
        let status = "Not Delivered";
        if (statusID !== 5) {
            status = language.hindi.notDelivered
        }
        if (statusID === 5) {
            status = language.hindi.delivered
        }
        return status;
    }

    getItemPaymentStatus = (statusID) => {
        let status = "Not Paid";
        if (statusID !== 5) {
            status = language.hindi.notPaid;
        }
        if (statusID === 5) {
            status = language.hindi.paid;
        }
        return status;
    }



    deleteItem = async (orderId, appType) => {
        const {
            orders
        } = this.state;

        const index = orders.findIndex(x => x.orderId === orderId);

        if (index !== -1) {
            orders[index].showSubmitProgressBar = true;
        }

        this.setState({ orders })


        const response = await DeleteMyOrders({ orderId, appType })
        if (response.hasError === false) {

            orders.splice(index, 1);

            this.setState({ orders })
            alert('Order Canceled')

        }
        else {
            apiResponse.hasError = response.hasError;
            apiResponse.message = response.message;
            orders[index].showSubmitProgressBar = false;
            this.setState({ apiResponse, orders })
            alert('Oops ,Problem')
        }

    }

    openRazorpay = (orderId, totalPrice) => {
        let {

            orders,
            user,

        } = this.state;
        const index = orders.findIndex(x => x.orderId === orderId);

        if (index !== -1) {
            orders[index].paymentProgressBar = true;
        }



        this.setState({ orders })
        this.GetOrderNo(totalPrice).then((id) => {
            var options = {
                description: 'Credits towards consultation',
                image: 'https://i.imgur.com/3g7nmJC.png',
                currency: 'INR',
                key: 'rzp_test_57XjyzhNYJTrtv',
                amount: totalPrice,
                name: 'KaamKaaj',
                order_id: id,//Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
                prefill: {
                    email: 'sample@sasdfsd.com',
                    contact: '8851057104',//user.mobileNumber,
                    name: user.name
                },
                theme: { color: '#53a20e' }
            }
            RazorpayCheckout.open(options).then((data) => {
                // handle success=

                if (data.razorpay_payment_id) {
                    UpdatePaymentOrderV1({ AppType: orders[index].appType, OrderId: orderId, PaymentMode: "Online RazorPay", PaymentDate: "", PaymentStatus: 5, PaymentTransactionNo: data.razorpay_payment_id }).then(x => {
                        if (x.hasError === false) {
                            orders[index].paymentStatus = 5;
                        }
                        this.setState({ orders, isPaymentSuccess: true, paymentTxnNo: data.razorpay_payment_id })

                    })
                }
                else {
                    UpdatePaymentOrderV1({ AppType: orders[index].appType, OrderId: orderId, PaymentMode: "Online RazorPay", PaymentDate: "", PaymentStatus: 2, PaymentTransactionNo: "Payment Failure" }).then(x => {

                        this.setState({ orders, isPaymentSuccess: true, paymentTxnNo: data.razorpay_payment_id })

                    })
                }


            }).catch((error) => {
                // handle failure
                // alert('fail is the out ocme')
                // this.setState({ showSubmitProgressBar: false })
                UpdatePaymentOrderV1({ AppType: orders[index].appType, OrderId: orderId, PaymentMode: "Online RazorPay", PaymentDate: "", PaymentStatus: 2, PaymentTransactionNo: `${error.code} | ${error.description}` }).then(x => {


                })

                alert(`Error: ${error.code} | ${error.description}`);
            });

        })




    }

    GetOrderNo = async (totalPrice) => {

        let request = {};
        request.amount = parseInt(totalPrice, 10) * 100   //5000;
        request.receipt = "SabKuch"
        let response = await getNewOrderID(request);
        if (response) {
            return response.id
        }
    }

    sortByDay = (itemValue, id) => {
        let {
            sort,
            orders,

        } = this.state;
        sort.days = itemValue;
        this.setState({ displayLoader: true })


        let filterdData = orders.filter(x => moment(x.createdOn.slice(0, 11)).isBetween(moment().add('d', -(itemValue)), moment()))
        orders = filterdData

        // orders.sort((x, y) => { return parseInt(x.totalPrice, 10) - parseInt(y.totalPrice, 10) })
        this.setState({ sort, orders })
        setInterval(() => { this.setState({ displayLoader: false }) }, 100)
    }

    sortByApp = (itemValue, id) => {
        let {
            sort,
            orders,
            unSortedOrders
        } = this.state;
        sort.apptype = itemValue;
        orders = []
        this.setState({ orders, displayLoader: true })
        orders = [...unSortedOrders];

        if (itemValue != "0") {
            let filterdData = orders.filter(x => x.appType == itemValue)
            orders = filterdData

        }
        this.setState({ orders }, () => { this.setState({ displayLoader: false }) })
        // setInterval(()=>{this.setState({displayLoader:false})},100)
    }

    getAppName = (id) => {

        const {
            Work: lang_Work,
            Tranport: lang_Transport,
            Hotel: lang_Hotel,
            Kirana: lang_Kirana,
            FruitVegitable: lang_FruitVegitable,
            off: lang_off

        } = DefaultScreenlanguage.hindi


        if (id === "3") {
            return lang_Work;
        }

        else if (id === "1") {
            return lang_Transport;
        }



        else if (id === "2") {
            return lang_Transport;
        }

        else if (id === "5") {
            return lang_Hotel;
        }

        else if (id === "6") {
            return lang_Kirana
        }

        else if (id === "7") {
            return lang_Transport
        }

        else if (id === "8") {
            return lang_FruitVegitable
        }
    }

    renderItems = () => {
        const {
            isSideBarDisplayed,
            vegitableFruitList,
            orders,
            sort,
            loadingArray,
            displayLoader,
            isCalenderDisplayed,
            createdOn,
            apiResponse,
            isNoRecord
        } = this.state;

        const {
            items: lang_items,
            orderno: lang_orderno,
            addressTxt: lang_addressTxt,
            deliveryStatus: lang_deliveryStatus,
            paymentStatus: lang_paymentStatus,
            cancel: lang_cancel,
            notDelivered: lang_notDelivered,
            notPaid: lang_notPaid,
            paid: lang_paid,
            delivered: lang_delivered,
            orderLoading: lang_orderLoading,
            noRecord:lang_noRecord

        } = language.hindi;


        if (displayLoader === true) {
            return (
                <View>
                    <View style={{ justifyContent: 'center', alignSelf: 'center',marginTop:30 }}><Text style={{ color: 'green', fontWeight: 'bold', fontSize: 16 }}>{lang_orderLoading}</Text></View>

                    {
                        loadingArray.map(x => {
                            return (
                                <View style={{
                                    backgroundColor: 'white',
                                    width: screenWidth,
                                    height: 90,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    borderRadius: 8,
                                    marginVertical: 5
                                }}>
                                    <View
                                        style={{
                                            height: 80, width: 150,
                                            backgroundColor: '#d3d3d3', margin: 5,
                                        }}
                                    >
                                    </View>
                                    <View style={{ justifyContent: 'space-around' }} >
                                        <View
                                            style={{
                                                height: 15, width: 100,
                                                backgroundColor: '#d3d3d3'
                                            }}
                                        >
                                        </View>
                                        <View
                                            style={{ height: 15, width: 100, backgroundColor: '#d3d3d3' }}
                                        >
                                        </View>
                                    </View>

                                    <View style={{ justifyContent: 'space-around' }}>
                                        <View
                                            style={{ height: 15, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                        >
                                        </View>
                                        <View
                                            style={{ height: 50, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                        >
                                        </View>
                                    </View>
                                </View>
                            )
                        })
                    }
                </View>
            )
        }

        else if (orders && orders.length === 0 && displayLoader === false) {
            return (
            <Text style={{ justifyContent: 'center', alignItems: 'center',color:'red',fontSize:16 }}>{lang_noRecord}</Text>
            )
        }
        else if (orders && orders.length > 0 && displayLoader === false) {
            return (
                orders.map(x => {
                    const {
                        orderId,
                        totalPrice,
                        isActive,
                        createdOn,
                        paymentStatus,
                        deliveryStatus,
                        customerRemarks,
                        agentRemarks,
                        userMobileNo,
                        appType,
                        address,
                        items
                    } = x;
                    return (

                        <View style={{ borderRadius: 8, margin: 5, backgroundColor: 'white', paddingVertical: 15, width: screenWidth }}>
                            <View style={{ alignSelf: 'center' }}><Text style={{ fontWeight: 'bold' }}>{`${createdOn.slice(0, 6)}  - ${this.getAppName(appType)} - \u20B9${totalPrice}`}</Text></View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ width: '34%', borderLeftWidth: 1, borderTopWidth: 1, borderRightWidth: 1, borderColor: 'black', borderBottomWidth: 1, justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 12 }}>
                                        {lang_orderno}
                                    </Text>
                                    <Text style={{ fontSize: 12 }}>
                                        {orderId}
                                    </Text>
                                </View>
                                <View style={{ width: '34%', borderRightWidth: 1, borderColor: 'black', borderBottomWidth: 1, borderTopWidth: 1 }}>
                                    <Text style={{ fontSize: 12, }}>
                                        {lang_deliveryStatus}
                                    </Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 12, marginRight: 10, color: `${deliveryStatus === 5 ? 'green' : 'red'}` }}>
                                            {this.getItemDeliveryStatus(deliveryStatus)}
                                        </Text>
                                        <Text>
                                            {deliveryStatus === 5 && <Image
                                                source={require('../cms/icons/common/Tick.png')}></Image>
                                            }
                                        </Text>
                                    </View>
                                </View>
                                <View style={{ width: '32%', borderTopWidth: 1, borderRightWidth: 1, borderColor: 'black', borderBottomWidth: 1, justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 12 }}>
                                        {lang_paymentStatus}
                                    </Text>
                                    <View style={{ flexDirection: 'row', marginRight: 10 }}>
                                        <Text>
                                            {this.getItemPaymentStatus(paymentStatus)}
                                        </Text>
                                        <Text>
                                            {paymentStatus === 5 && <Image
                                                source={require('../cms/icons/common/Tick.png')}></Image>
                                            }
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', width: '100%', borderBottomWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>
                                <Text style={{ fontSize: 12, marginRight: 10 }}>{lang_items}</Text>
                                <Text style={{ fontSize: 12, flexWrap: 'wrap' }}>{items}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', width: '100%', borderBottomWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor: 'black' }}>
                                <Text style={{ fontSize: 12, marginRight: 10 }}>{lang_addressTxt}</Text>
                                <Text style={{ fontSize: 12, flexWrap: 'wrap', marginRight: 5 }}>{address}</Text>

                            </View>

                            <View style={{
                                margin: 3, flexDirection: 'row',
                                justifyContent: 'space-between',
                                display: `${deliveryStatus === 5 ? 'none' : 'flex'}`
                            }}>



                                {/* {paymentStatus !== 5 && <TouchableOpacity
                                    onPress={() => {
                                        this.openRazorpay(orderId, totalPrice);

                                    }}

                                    style={{ flexDirection: 'row', marginHorizontal: 10, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', height: 40, width: 100, borderRadius: 10 }}>
                                    <Text style={{ color: 'white', fontSize: 20 }}>{`\u20B9 ${totalPrice} PAY`}</Text>
                                    <ProgressBarAndroid style={{ display: `${x.paymentProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                                </TouchableOpacity>
                                } */}
                                {deliveryStatus !== 5 && <TouchableOpacity
                                    onPress={() => this.deleteItem(orderId, appType)}
                                    style={{ flexDirection: 'row', alignSelf: 'flex-end', height: 30, width: 100, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
                                    <Text style={{ color: 'white' }}>{lang_cancel}</Text>
                                    <ProgressBarAndroid style={{ display: `${x.showSubmitProgressBar ? 'flex' : 'none'}`, height: 15, width: 15 }} color="#ffffff" />
                                </TouchableOpacity>}
                            </View>



                            <View style={{ display: `${apiResponse.hasError === true ? 'flex' : 'none'}` }}>
                                <Text style={{ color: 'red' }}>{apiResponse.message}</Text>
                            </View>
                        </View>
                    )

                })

            )
        }


    }

    render() {
        const {
            isSideBarDisplayed,
            vegitableFruitList,
            orders,
            sort,
            loadingArray,
            displayLoader
        } = this.state;






        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <ScrollView style={{ height: screenHeight, backgroundColor: 'pink' }}>

                    {/* <View
                        onTouchEnd={() => { this.setState({ isSideBarDisplayed: false }) }}

                        style={{ backgroundColor: 'white', margin: 10, padding: 5, width: screenWidth, marginTop: 40 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', padding: 5 }}>

                            <Image style={{ tintColor: 'red', width: 45, height: 40 }}

                                source={{ uri: 'https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/icons/Filter.png' }}
                            ></Image>

                            <View style={{ flexDirection: 'row', width: 120, height: 45, borderColor: 'black', borderWidth: 1 }}>
                                <View style={{ position: 'relative' }}>
                                    <Text style={{ width: 120, fontSize: 12, paddingHorizontal: 10 }}>Sort by Day</Text>
                                    <Picker
                                        selectedValue={sort.days}
                                        style={{ position: 'absolute', width: 120, height: 55 }}
                                        mode="dropdown"
                                        onValueChange={(itemValue, itemIndex) => this.sortByDay(itemValue, itemIndex)}
                                    >
                                        <Picker.Item label="All" value={0} />
                                        <Picker.Item label="1 Week" value={7} />
                                        <Picker.Item label="15 Day" value={15} />
                                        <Picker.Item label="1 Month" value={30} />
                                        <Picker.Item label="2 Month" value={60} />
                                        <Picker.Item label="6 Month" value={180} />
                                        <Picker.Item label="1 Year" value={365} />
                                    </Picker>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', width: 120, height: 45, borderColor: 'black', borderWidth: 1 }}>
                                <View style={{ position: 'relative' }}>
                                    <Text style={{ width: 120, fontSize: 12, paddingHorizontal: 10 }}>Sort by App</Text>
                                    <Picker
                                        selectedValue={sort.apptype}
                                        style={{ position: 'absolute', width: 120, height: 55 }}
                                        mode="dropdown"
                                        onValueChange={(itemValue, itemIndex) => this.sortByApp(itemValue, itemIndex)}
                                    >
                                        <Picker.Item label="All" value="0" />
                                        <Picker.Item label="VegitableLAndFruit" value="8" />
                                        <Picker.Item label="Kirana" value="3" />
                                        <Picker.Item label="Hotel" value="7" />
                                        <Picker.Item label="Transport" value="6" />
                                        <Picker.Item label="Work" value="5" />
                                        <Picker.Item label="Shopping" value="1" />

                                    </Picker>
                                </View>
                            </View>

                        </View>


                    </View> */}



                    <View
                        onTouchEnd={() => { this.setState({ isSideBarDisplayed: false }) }}
                        style={{ marginTop: 40 }}>
                        {
                            this.renderItems()

                            // orders && orders.length > 0 && displayLoader === false ? orders.map(x => {
                            //     const {
                            //         orderId,
                            //         totalPrice,
                            //         isActive,
                            //         createdOn,
                            //         paymentStatus,
                            //         deliveryStatus,
                            //         customerRemarks,
                            //         agentRemarks,
                            //         appType,
                            //         items,
                            //         userMobileNo,
                            //         address,

                            //     } = x;
                            //     return (

                            //         <View style={{ borderRadius: 8, margin: 10, backgroundColor: 'white', paddingHorizontal: 10, width: screenWidth }}>
                            //             <View>
                            //                 <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            //                     <Text style={{ fontSize: 16 }}>
                            //                         {`OrderNo - ${orderId}`}
                            //                     </Text>
                            //                     <Text style={{ fontSize: 14, fontWeight: '700' }}>
                            //                         {createdOn.slice(0, 11)}
                            //                         {/* {`${orderId.slice(6, 8)}-${this.getMonth(orderId.slice(4, 6))}-${orderId.substring(0, 4)}`} */}
                            //                     </Text>
                            //                 </View>
                            //                 <Text style={{ fontSize: 16, fontWeight: '700' }}>
                            //                     {`Delivery Status - ${this.getItemDeliveryStatus(deliveryStatus)}`}
                            //                 </Text>
                            //                 <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            //                     <Text style={{ fontSize: 16, fontWeight: '700' }}>
                            //                         {`Payment Status - ${this.getItemPaymentStatus(paymentStatus)}`}
                            //                     </Text>
                            //                     {paymentStatus == "5" && <Image
                            //                         source={require('../cms/icons/common/Tick.png')}></Image>

                            //                     }
                            //                 </View>
                            //             </View>
                            //             <View>
                            //                 <Text style={{ fontSize: 16, fontWeight: '700' }}>Items-{items}</Text>

                            //             </View>
                            //             <View>
                            //                 <Text style={{ fontSize: 16, fontWeight: '700' }}>{`Total - \u20B9${totalPrice}`}</Text>

                            //             </View>
                            //             <View style={{ margin: 7 }}>
                            //                 {paymentStatus != "5" && <TouchableOpacity
                            //                     onPress={() => {
                            //                         this.openRazorpay(orderId, totalPrice);

                            //                     }}

                            //                     style={{ flexDirection: 'row', marginHorizontal: 10, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', height: 40, width: 100, borderRadius: 10 }}>
                            //                     <Text style={{ color: 'white', fontSize: 20 }}>{`\u20B9 ${totalPrice} PAY`}</Text>
                            //                     <ProgressBarAndroid style={{ display: `${x.paymentProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                            //                 </TouchableOpacity>
                            //                 }
                            //                 <TouchableOpacity
                            //                     onPress={() => this.deleteItem(orderId, appType)}
                            //                     style={{ flexDirection: 'row', alignSelf: 'flex-end', height: 30, width: 100, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
                            //                     <Text style={{ color: 'white' }}>Cancel</Text>
                            //                     <ProgressBarAndroid style={{ display: `${x.showSubmitProgressBar ? 'flex' : 'none'}`, height: 15, width: 15 }} color="#ffffff" />
                            //                 </TouchableOpacity>
                            //             </View>

                            //         </View>
                            //     )

                            // }) : loadingArray.map(x => {
                            //     return (
                            //         <View style={{
                            //             backgroundColor: 'white',
                            //             width: screenWidth,
                            //             height: 90,
                            //             flexDirection: 'row',
                            //             justifyContent: 'space-between',
                            //             borderRadius: 8,
                            //             marginVertical: 5



                            //         }}>

                            //             <View
                            //                 style={{
                            //                     height: 80, width: 150,
                            //                     backgroundColor: '#d3d3d3', margin: 5,

                            //                 }}
                            //             >

                            //             </View>

                            //             <View style={{ justifyContent: 'space-around' }} >
                            //                 <View
                            //                     style={{
                            //                         height: 15, width: 100,

                            //                         backgroundColor: '#d3d3d3'
                            //                     }}
                            //                 >

                            //                 </View>
                            //                 <View
                            //                     style={{ height: 15, width: 100, backgroundColor: '#d3d3d3' }}
                            //                 >

                            //                 </View>

                            //             </View>

                            //             <View style={{ justifyContent: 'space-around' }}>
                            //                 <View
                            //                     style={{ height: 15, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                            //                 >

                            //                 </View>
                            //                 <View
                            //                     style={{ height: 50, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                            //                 >

                            //                 </View>
                            //             </View>
                            //         </View>
                            //     )

                            // })

                        }

                    </View>
                </ScrollView>
            </SafeAreaView>

        )
    }

}

const mapStatetoProps = state => {
    return {
        state
    }
}

const mapDispatchtoProps = dispatch => {
    return {
        saveUserData: data => dispatch(saveUserData(data))
    };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(MyOrder);
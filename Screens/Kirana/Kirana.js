import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid,
    Dimensions,
    AsyncStorage,
    BackHandler
} from 'react-native';

import SideBar from '../../Component/SideBar';

import Header from '../../Component/Header';

import language from '../../cms/lang/language';

import { GetKiranaList, CreateUpdateKiranaOrder } from '../../Service/Kaamkaaj';

import { TouchableHighlight } from 'react-native-gesture-handler';

import { SliderBox } from "react-native-image-slider-box";
import { connect } from 'react-redux';
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen')



class Kirana extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kiranaList: [],
            isSideBarDisplayed: false,
            selectedItem: [],
            selectedValue: 1,
            showItemNumber: false,
            showSearchingCard: false,
            showBookedWorkerCard: false,
            userTotalPrice: 0,
            showSubmitProgressBar: false,
            userkiranaList: [],
            num: [],
            createUpdateKiranarequest: {
                userKiranaOrders: {
                    OrderId: null,
                    TotalPrice: "900",
                    UserId: "12345",
                    IsActive: "1",
                    CreatedOn: null,
                    UpdatedOn: null,
                    PaymentStatus: "1",
                    DeliveryStatus: "2",
                    CustomerRemarks: "",
                    AgentRemarks: "",
                    Address: "fkjds",
                    UserMobileNo: "kkkk",
                    Items: "jhbjkjnkj"
                },
                kiranaOrderItems: [
                    {
                        Id: null,
                        OrderNo: "fsdfsd",
                        MobileNo: "45654",
                        UserId: "456754",
                        ItemId: "r456765",
                        ItemName: "45676",
                        ItemUnit: "4567",
                        ItemPrice: "4567",
                        ItemQuantity: "9876",
                        IsActive: "1",
                        CreatedOn: "djlf",
                        Zorder: "2345"
                    }
                ]
            },
            loadingArray: [1, 2, 3, 4, 5, 6, 7]

        }
    }

    backAction = () => {

        this.props.navigation.navigate('Default')
    };

    async componentDidMount() {
        // this.backHandler = BackHandler.addEventListener(
        //     "hardwareBackPress",
        //     this.backAction()
        // );

        const {
            num
        } = this.state;
        const result = await GetKiranaList();


        result.kiranaItems.map(x => {
            x.quantity.sort((x, y) => {
                return x - y
            })

            x.itemQuantity = Math.min(...x.quantity)
        })
        if (result) {

            this.setState({
                kiranaList: result.kiranaItems
            })
        }


    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction());
    }



    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    handelItemQuantity = (x) => {
        const {
            selectedItem,

            kiranaList
        } = this.state;
        const index = kiranaList.findIndex(x => x.id === selectedItem);
        if (index !== -1) {
            const price = kiranaList[index].price / kiranaList[index].quantity;
            kiranaList[index].price = price * x;
            kiranaList[index].quantity = x
        }
        this.setState({ showItemNumber: false, kiranaList })
    }

    setQuantity = (quantity, id) => {
        const {
            kiranaList
        } = this.state;
        const itemIndex = kiranaList.findIndex(x => x.id === id);
        if (itemIndex !== -1) {
            kiranaList[itemIndex].itemQuantity = quantity;
            kiranaList[itemIndex].totalPrice = kiranaList[itemIndex].price * quantity

        }
        this.setState(kiranaList);

    }

    addItem = async (id, itemQuantity, unit, price, name) => {

        let {
            userkiranaList,
            userTotalPrice
        } = this.state;

        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);
        }
        if (userkiranaList && userkiranaList.length > 0) {
            const index = userkiranaList.findIndex(x => x.itemId === id)
            if (index !== -1) {
                userkiranaList[index].itemQuantity = userkiranaList[index].itemQuantity + itemQuantity;
                userkiranaList[index].itemPrice = userkiranaList[index].itemPrice + (itemQuantity * price);
            }
            else {
                userkiranaList.push({ id: 0, orderNo: 0, mobileNo: user.mobileNumber, userId: user.id, itemId: id, itemName: name, itemUnit: unit, itemPrice: itemQuantity * price, itemQuantity, isActive: 1, zorder: 1 })

            }
        }
        else {
            userkiranaList.push({ id: 0, orderNo: 0, mobileNo: user.mobileNumber, userId: user.id, itemId: id, itemName: name, itemUnit: unit, itemPrice: itemQuantity * price, itemQuantity, isActive: 1, zorder: 1 })

        }



        userTotalPrice += itemQuantity * price;

        this.setState({ userkiranaList, userTotalPrice });

    }
    removeSelectedItem = (id, quantity, totalPrice) => {
        let {
            userTotalPrice,
            userkiranaList
        } = this.state;
        const index = userkiranaList.findIndex(x => x.itemId === id)
        if (index !== -1) {
            userTotalPrice -= totalPrice;
            userkiranaList.splice(index, 1)
        }
        this.setState({ userkiranaList, userTotalPrice })

    }



    orderNow = async () => {
        const {
            userkiranaList,
            userTotalPrice,
            showSubmitProgressBar,
            createUpdateKiranarequest
        } = this.state;
        this.setState({ showSubmitProgressBar: true })

        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);

            let itemsList = "";
            userkiranaList.map(x => {
                itemsList += `${x.itemName} ${x.itemQuantity} ${x.itemUnit} \u20B9${x.itemPrice} ,`
            })
            createUpdateKiranarequest.kiranaOrderItems = userkiranaList

            createUpdateKiranarequest.userKiranaOrders =
                { OrderId: 0, TotalPrice: userTotalPrice, UserId: user.id, IsActive: 1, PaymentStatus: 1, DeliveryStatus: 1, CustomerRemarks: "", AgentRemarks: "", Address: `${user.address.address1} ${user.address.address2} ${user.address.city} ${user.address.pincode} `, UserMobileNo: user.mobileNumber, Items: itemsList }

        }


        let response = await CreateUpdateKiranaOrder(createUpdateKiranarequest);
        if (response) {

            this.setState({ showSubmitProgressBar: false })
            let user = await AsyncStorage.getItem("userLocalStorage", null)

            if (user) {
                user = JSON.parse(user);
                this.props.navigation.navigate('BookingConfirmation', { image: "image0", appType: "6", price: userTotalPrice, items: response.kiranaOrderItems, orderNo: response.userKiranaOrders.orderId, user })

            }
        }
        else {
            this.setState({ showSubmitProgressBar: false })
        }


    }

    getUnitLang = (val) => {
        if (val.toLowerCase() === "kg") {
            return val.replace(val, language.hindi.kg);
        }
        else {
            return val
        }


    }

    isItemAdded = (id) => {
        let isExist = false;
        const {
            userkiranaList
        } = this.state;

        const index = userkiranaList.findIndex(x => x.itemId === id)
        if (index !== -1) {
            isExist = true;
        }
        return isExist;
    }

    render() {
        const {
            showSubmitProgressBar,
            kiranaList,
            isSideBarDisplayed,
            showItemNumber,
            num,
            showSearchingCard,
            showBookedWorkerCard,
            userkiranaList,
            userTotalPrice,
            loadingArray

        } = this.state;

        const {
            add: lang_add,
            kg: lang_Kg,
            orderNow: lang_orderNow,
            listLoading:lang_listLoading
        } = language.hindi;

        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <View style={{ backgroundColor: 'maroon', marginTop: 20, display: `${userkiranaList.length > 0 ? 'flex' : 'none'}` }}>

                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 30 }}>
                        {userkiranaList.map(x => {
                            const {
                                itemName,
                                itemPrice,
                                itemQuantity,
                                itemUnit,
                                itemId

                            } = x;
                            return (

                                <View style={{ flexDirection: 'row', position: 'relative' }}>
                                    <View style={{ backgroundColor: 'green', padding: 5, marginHorizontal: 3, marginTop: 10 }}>
                                        <Text style={{ color: 'white', fontSize: 10 }}>{`${itemName} ${itemQuantity}${itemUnit} - \u20B9 ${itemPrice}`}</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={{ position: 'absolute', left: 20, top: 0, width: 20, height: 20, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}
                                        onPress={() => this.removeSelectedItem(itemId, itemQuantity, itemPrice)}
                                    >
                                        <Text style={{ color: 'white', fontSize: 16 }}>X</Text>
                                    </TouchableOpacity>

                                </View>)
                        })
                        }

                    </View>
                    <View style={{ alignSelf: 'center', marginTop: 20 }}>
                        <TouchableOpacity
                            style={{ flexDirection: 'row', backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', padding: 10, borderRadius: 8 }}
                            onPress={() => {
                                this.orderNow()
                            }}

                        >
                            <Text style={{ marginRight: 5, color: 'white' }}>{'\u20B9'} {userTotalPrice} {lang_orderNow}</Text>
                            <ProgressBarAndroid style={{ display: `${showSubmitProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                        </TouchableOpacity>
                    </View>
                </View>

                <ScrollView style={styles.body}>
                    <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                        <View style={{ flex: 1 }}>

                            <View style={{ backgroundColor: '#84C2F9', flex: 1, flexDirection: 'column' }}>


                                <View style={{ marginTop: 30, marginHorizontal: 5 }}>
                                    {
                                        kiranaList && kiranaList.length > 0 ? kiranaList.map((x, index) => {
                                            const {
                                                name,
                                                id,
                                                description,
                                                description1,
                                                price,
                                                quantity,
                                                itemQuantity,
                                                photo,
                                                photo1,
                                                photo2,
                                                unit,
                                                createdOn,
                                                zOrder,
                                                type,
                                                isActive,
                                                hindiName
                                            } = x;
                                            return (
                                                <View
                                                    style={[styles.chips]}
                                                >
                                                    <View style={{ width: 130, marginHorizontal: 5 }}>

                                                        <SliderBox
                                                            images={[photo]}
                                                            onCurrentImagePressed={() => { this.addItem(id, itemQuantity, unit, price, hindiName) }}
                                                            sliderBoxHeight={80}
                                                            parentWidth={130}

                                                            dotColor="#FFEE58"
                                                            inactiveDotColor="white"
                                                            autoplay

                                                            dotStyle={{
                                                                width: 15,
                                                                height: 15,
                                                                borderRadius: 15,
                                                                marginHorizontal: 10,
                                                                padding: 0,
                                                                margin: 0
                                                            }}

                                                        />
                                                    </View>


                                                    <View style={{ width: `30%` }}>
                                                        <Text style={{ color: 'black', fontSize: 16 }}>
                                                            {/* {language.hindi.workMaster.filter(x => x.id == id)[0].value} */}
                                                            {hindiName}
                                                        </Text>
                                                        <View style={{ flexDirection: 'row' }}>

                                                            <Picker
                                                                selectedValue={itemQuantity}
                                                                style={{ borderWidth: 2, borderColor: 'black', height: 40, width: 75, borderStartWidth: 3 }}
                                                                onValueChange={(itemValue, itemIndex) => this.setQuantity(itemValue, id)}
                                                                mode='dropdown'
                                                            >
                                                                {
                                                                    quantity.map(x => {
                                                                        return (<Picker.Item label={`${x}`} value={x} />)
                                                                    })
                                                                }
                                                            </Picker>

                                                        </View>
                                                    </View>


                                                    <View style={{ width: "30%", flexDirection: 'column' }}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                                            <Text style={{ color: 'black', fontSize: 16 }}>{`\u20B9${price}/${this.getUnitLang(unit)}`}</Text>

                                                            {this.isItemAdded(id) && <Image style={styles.horizontalTick} source={require('../../cms/icons/common/Tick.png')}></Image>}

                                                        </View>
                                                        <TouchableOpacity
                                                            onPress={() => { this.addItem(id, itemQuantity, unit, price, hindiName) }}
                                                            style={{ marginVertical: 10, height: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0A1AF5' }}>
                                                            <Text style={{ color: 'white', fontSize: 16 }}>{lang_add}</Text>
                                                        </TouchableOpacity>

                                                    </View>

                                                </View>
                                            )
                                        }) : <View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ color: 'green', fontSize: 20 }}>{lang_listLoading}</Text>
                                        </View>
                                        <View>{
                                            loadingArray.map(x => {
                                                return (
                                                    <View style={{
                                                        backgroundColor: 'white',
                                                        width: screenWidth,
                                                        height: 90,
                                                        flexDirection: 'row',
                                                        justifyContent: 'space-between',
                                                        borderRadius: 8,
                                                        marginVertical: 5
                                                    }}>

                                                        <View
                                                            style={{
                                                                height: 80, width: 150,
                                                                backgroundColor: '#d3d3d3', margin: 5,

                                                            }}
                                                        >

                                                        </View>

                                                        <View style={{ justifyContent: 'space-around' }} >
                                                            <View
                                                                style={{
                                                                    height: 15, width: 100,

                                                                    backgroundColor: '#d3d3d3'
                                                                }}
                                                            >

                                                            </View>
                                                            <View
                                                                style={{ height: 15, width: 100, backgroundColor: '#d3d3d3' }}
                                                            >

                                                            </View>

                                                        </View>

                                                        <View style={{ justifyContent: 'space-around' }}>
                                                            <View
                                                                style={{ height: 15, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                                            >

                                                            </View>
                                                            <View
                                                                style={{ height: 50, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                                            >

                                                            </View>
                                                        </View>
                                                    </View>
                                                )

                                            })
                                        }</View>
                                    </View>



                                    }
                                </View>
                            </View>

                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </SafeAreaView>
        )
    }
}


const mapStatetoProps = state => {
    return {
        state
    }
}

const mapDispatchtoProps = dispatch => {
    return {
        saveUserData: data => dispatch(saveUserData(data))
    };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(Kirana);

const styles = StyleSheet.create({

    body: {
        marginTop: 40,

    },
    chips: {

        // borderWidth: 1,
        // borderColor: '#f0f0f0',
        borderRadius: 8,
        backgroundColor: 'white',
        // width: 195,
        height: 90,
        marginVertical: 5,

        flexDirection: 'row'

    },
    example: {
        marginVertical: 0,
    },
    activeChips: {
        backgroundColor: 'green'
    },
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },
    card: {

        // backgroundColor: 'white',
        // borderRadius: 8,
        // height: 300,
        // marginBottom: 15,
        // overflow: 'scroll',


    },
    label: {
        fontSize: 16
    },
    textbox: {
        borderRadius: 18,
        borderWidth: 1,
        width: '100%',
        height: 50,
        fontSize: 16
    },
    submitBtn: {
        width: 164,
        height: 46,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'red',
        fontSize: 16
    }
});

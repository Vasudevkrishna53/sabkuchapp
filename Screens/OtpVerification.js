import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    ProgressBarAndroid,
    Platform,
    Image,
    PermissionsAndroid,
    Dimensions,
    TouchableHighlight,
    AsyncStorage

} from 'react-native';
import {
    verifyMobileNo
} from '../Service/Kaamkaaj';

const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;

class OtpVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userMobileNo: '',
            otp: '',
            errorMsg:'',
            showSubmitProgressBar:false
        }
    }

  

    verifyUserMobileNo = async () => {
        let {
            otp,
            showSubmitProgressBar,
            errorMsg
        } = this.state;

        const {
            user
        } = this.props;

        const {
            mobileNumber
        } = user;

        this.setState({showSubmitProgressBar:true})

        if (mobileNumber && otp) {
            let result = await verifyMobileNo({ mobileNumber, mobileNoOtp: otp })
            if (result.isMatch === 1) {
                showSubmitProgressBar=false;
                this.props.navigation.navigate('Default');
            }
            else{
                errorMsg='Oops ! Wrong Otp,try again' 
            }
        }
        else{
            errorMsg='Oops ! Wrong Otp,try again' 
        }
        this.setState({
            showSubmitProgressBar:false,errorMsg
        })

         setTimeout(()=>this.setState({errorMsg:''}),10000)
    }

    handelOtp = (text) => {
        this.setState({ otp: text })
    }

    render() {
        const {
            otp,
            showSubmitProgressBar,
            errorMsg
        } = this.state;
        const {
            user
        } = this.props;

        const {

            name,
            mobileNumber
        } = user;
        return (
            <View style={{ width: screenWidth, height: screenHeight, backgroundColor: '#F5CACA' }}>

                <View style={{ marginVertical: 10, backgroundColor: 'white', borderRadius: 12, marginHorizontal: 10, height: 150 }}>
                    <View>
                        <Text style={{ alignSelf: 'center', color: 'green', fontSize: 22 }}>Thanks {name} , you have become</Text>
                        <Text style={{ alignSelf: 'center', color: 'green', fontSize: 20 }}>a family now</Text>
                        <Text style={{ color: 'red', fontSize: 20 }}>You will receive a otp on {mobileNumber} in 30 Sec for mobile number verification</Text>

                    </View>
                </View>

                <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 12, marginHorizontal: 10, marginVertical: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>ENTER OTP</Text>
                    <TextInput
                        autoFocus={true}
                        value={otp}
                        keyboardType="numeric"
                        maxLength={4}
                        onChangeText={(text) => { this.handelOtp(text) }}
                        style={{ fontSize: 20, fontWeight: '700', borderBottomColor: 'black', borderBottomWidth: 2, width: 100, paddingBottom: 0 }}
                    >

                    </TextInput>

                    <TouchableOpacity
                        style={{ flexDirection:'row',borderRadius: 8, margin: 40, width: 100, height: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: 'green' }}
                        onPress={() => this.verifyUserMobileNo()}
                    >
                        <Text style={{ color: 'white', fontSize: 20 }}>Submit</Text>
                        <ProgressBarAndroid style={{ display: `${showSubmitProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                    </TouchableOpacity>
        <Text style={{color:'red',fontSize:20,fontWeight:'700'}}>{errorMsg}</Text>


                </View>


                <View style={{ marginVertical: 10, height: 150, backgroundColor: 'white', borderRadius: 12, marginHorizontal: 10 }}>
                    <Text style={{ color: 'red', fontSize: 20 }}>If you do not receive OTP in 30 Sec then click here to resend or change your mobileNumber</Text>
                </View>

            </View>
        )
    }
}

const mapStatetoProps = state => {
    // console.log("value in state is ------",state)
    return {
        user: state.user
    }
}

// export default OtpVerification;

export default connect(mapStatetoProps, null)(OtpVerification);


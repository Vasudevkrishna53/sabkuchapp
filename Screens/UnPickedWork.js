import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Alert,
    TouchableWithoutFeedback
} from 'react-native';

import language from '../cms/lang/language'
import lang from '../cms/lang/language';
import { getUnpickedWork } from '../Service/Kaamkaaj';
import Header from '../Component/Header';
import SideBar from '../Component/SideBar';

export default class UnpickedWork extends Component {
    constructor(props) {
        super(props);
        this.state = {
            unpickedWorks: [],
            isSideBarDisplayed: false

        }
    }
    async componentDidMount() {
        let data = await getUnpickedWork();
        if (data && data.length > 0) {

            this.setState({
                unpickedWorks: data
            })
        }

    }
    unpickedWork = async () => {

    }


    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    render() {
        const {
            unpickedWorks,
            isSideBarDisplayed
        } = this.state;
        return (
            <SafeAreaView>

                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <View>
                    <Header
                        handelMenuDisplayer={this.handelMenuDisplayer}
                    />
                    <ScrollView>
                        <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>

                            <View style={{ flex: 1 }}>
                                <View style={{ marginLeft: 10, marginRight: 10, flex: 1, flexDirection: 'column' }}>
                                    <View style={{ marginTop: 15 }}>

                                        <TextInput placeholder="Search" style={styles.textbox}></TextInput>
                                    </View>


                                    <View style={{ marginTop: 15 }}>
                                        {
                                            unpickedWorks && unpickedWorks.map(x => {
                                                const {
                                                    id,
                                                    workID,
                                                    name,
                                                    description,
                                                    address
                                                } = x;
                                                return (
                                                    <View style={styles.card} >
                                                        <View style={{ flexDirection: 'row', margin: 10 }}>
                                                            <View>
                                                                <Text>
                                                                    Work Title
                                            </Text>
                                                                <Text>
                                                                    {name}
                                                                </Text>
                                                            </View>
                                                            <Text style={{ marginLeft: 120 }}>
                                                                Work location
                                            </Text>
                                                        </View>
                                                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', margin: 10 }}>
                                                            <Text>
                                                                Work Description
                                                            </Text>
                                                            <View style={{ position: 'absolute', bottom: 0, left: 275, marginBottom: 10 }}>
                                                                <TouchableOpacity onPress={() => { this.props.navigation.navigate('WorkOwnerDetail') }} style={styles.submitBtn}>
                                                                    <Text style={styles.text}>Book Now</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>

                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    body: {

    },
    card: {
        backgroundColor: 'white',
        borderRadius: 24,
        height: 90,
        marginBottom: 15,

    },
    label: {
        fontSize: 24
    },
    textbox: {
        borderRadius: 130,
        borderWidth: 2,
        width: '100%',
        height: 67,
        fontSize: 24,
        borderColor: 'white',
        backgroundColor: 'white'

    },
    submitBtn: {
        // width: 100,
        // height: 40,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,


        // marginLeft: 120,
    },
    text: {
        color: 'white',
        fontSize: 16
    }
});

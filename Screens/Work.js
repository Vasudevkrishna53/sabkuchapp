import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid
} from 'react-native';

import SideBar from '../Component/SideBar';
import Header from '../Component/Header';

import language from '../cms/lang/language';
import { getWorksMaster, createUpdateWork } from '../Service/Kaamkaaj';
import { TouchableHighlight } from 'react-native-gesture-handler';



export default class Work extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showWorkModal: false,
            workMaster: [],
            isSideBarDisplayed: false,
            user: {
                id: "288179d0-d2b8-4d6d-9b2a-d8e1bd8d6860"
            },
            selectedWork: [],
            showCurrentAddress: true,
            showEditAddress: false,
            address1: '',
            address2: '',
            pincode: '',
            state: '',
            selectedValue: 1,
            showWorkerNumber: false,
            num: [],
            showSearchingCard: false,
            showBookedWorkerCard: false
        }
    }

    async componentDidMount() {
        const {
            num
        } = this.state;
        const result = await getWorksMaster();


        result && result.map(x => {
            x.quantity = 1;
        })
        if (result && result.length > 0) {
            result.sort(function (x, y) {
                if (x.zOrder < y.zOrder) {
                    return -1;
                }
                else {
                    return 1;
                }
            })
            this.setState({
                workMaster: result
            })
        }

        let i = 1;
        for (i; i < 20; i++) {
            num.push(i)
        }
        this.setState({ num })
    }

    async createUpdateWork() {

        let result = "";

        const {
            user,
            selectedWork
        } = this.state
        let response = await createUpdateWork({ user, selectedWork })

        if (response) {
            return response;
        }
        else {
            return result;
        }
    }

    handelWorkPress = async (id) => {

        const {
            workMaster,
            selectedWork,

        } = this.state;
        this.setState({
            showBookedWorkerCard: false,
            showSearchingCard: true
        })

        const index = workMaster.findIndex(x => x.id === id)
        if (index != -1) {
            workMaster[index].isActive = !workMaster[index].isActive;
            if (workMaster[index].isActive === true) {
                selectedWork.push({ workID: workMaster[index].id, name: workMaster[index].name });
            }
            else {
                if (selectedWork.length > 0) {
                    const removeIndex = selectedWork.findIndex(x => x.workID === id)
                    selectedWork.splice(removeIndex, 1);
                }
            }

        }

        let response = await this.createUpdateWork();
        if (response) {
            this.setState({
                showSearchingCard: false,
                showBookedWorkerCard: true
            })
        }
        else {
            this.setState({
                showSearchingCard: false,

            })
        }

        this.setState({
            workMaster,
            selectedWork
        });

    }

    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    handelWorkerNumber = (x) => {
        const {
            selectedWork,
            workMaster
        } = this.state;
        const index = workMaster.findIndex(x => x.id === selectedWork);
        if (index !== -1) {
            const price = workMaster[index].price / workMaster[index].quantity;
            workMaster[index].price = price * x;
            workMaster[index].quantity = x
        }
        this.setState({ showWorkerNumber: false, workMaster })
    }

    render() {
        const {
            showWorkModal,
            workMaster,
            selectedWork,
            isSideBarDisplayed,
            showCurrentAddress,
            showEditAddress,
            address1,
            address2,
            pincode,
            state,
            selectedValue,
            showWorkerNumber,
            num,
            showSearchingCard,
            showBookedWorkerCard
        } = this.state;




        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <ScrollView style={styles.body}>


                    <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                        <View style={{ flex: 1 }}>

                            <View style={{ backgroundColor: '#84C2F9', flex: 1, flexDirection: 'column' }}>
                                
                                <View style={{ marginTop: 30, backgroundColor: 'white', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', marginHorizontal: 5 }}>
                                    {
                                        workMaster && workMaster.map((x, index) => {
                                            const {
                                                name,
                                                id,
                                                price,
                                                quantity,
                                                image0
                                            } = x;
                                            return (
                                                <View
                                                    style={[styles.chips]}
                                                // onPress={() => { this.handelWorkPress(id) }}
                                                >
                                                    <View style={{ marginHorizontal: 5 }}>
                                                        <Image style={{ width: 150, height: 190 }} source={{ uri: image0 }}></Image>



                                                        <Text style={{ color: 'black', fontSize: 24, fontWeight: 'bold' }}>
                                                            {language.hindi.workMaster.filter(x => x.id == id)[0].value}
                                                        </Text>

                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                                            <TouchableOpacity
                                                                onPress={() => {
                                                                    this.setState({ showWorkerNumber: true, selectedWork: id })
                                                                }}
                                                            >
                                                                <View style={{ flexDirection: 'row', backgroundColor: 'white', marginRight: 2, zIndex: 5 }}>
                                                                    <Text style={{ color: 'black', fontSize: 20 }}>{quantity}</Text>
                                                                    <Text style={{ color: 'black', fontSize: 20, transform: [{ rotate: '90deg' }] }}>{'\u2023'}</Text>

                                                                </View>
                                                            </TouchableOpacity>

                                                            <TouchableOpacity
                                                            onPress={()=>{this.props.navigation.navigate('BookingConfirmation',{image:image0,workName:name,price})}}
                                                                style={{position:'absolute',right:0, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', width: 100, height: 40, backgroundColor: '#0A1AF5', fontWeight: 'bold' }}>
                                                                <Text style={{ color: 'white', fontSize: 22 }}>{'\u20B9'}{price}</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>

                           
                          
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    body: {
        marginTop: 40,

    },
    chips: {

        borderWidth: 1,
        borderColor: '#f0f0f0',
        borderRadius: 8,
        backgroundColor: 'white',
        width: 195,
        height: 268,

    },
    example: {
        marginVertical: 0,
    },
    activeChips: {
        backgroundColor: 'green'
    },
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },
    card: {

        // backgroundColor: 'white',
        // borderRadius: 8,
        // height: 300,
        // marginBottom: 15,
        // overflow: 'scroll',


    },
    label: {
        fontSize: 24
    },
    textbox: {
        borderRadius: 18,
        borderWidth: 1,
        width: '100%',
        height: 50,
        fontSize: 24
    },
    submitBtn: {
        width: 164,
        height: 46,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'red',
        fontSize: 24
    }
});

import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    ProgressBarAndroid,
    Platform,
    Image,
    PermissionsAndroid,
    Dimensions,
    TouchableHighlight,
    AsyncStorage,
    Picker

} from 'react-native';

import {
    GetLoginUser,

} from '../../Service/Kaamkaaj';
import { saveUserData } from '../../store/reducer/kaamKaajActiion'

import language from '../../cms/lang/language'



const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mobileNumber: null,
            password: null,
            logInFailed: ''
        }



    }










    handelmobileNo = (text) => {
        let {
            mobileNumber
        } = this.state;

        text = text.replace(/([^\d]+)/g, "")

        mobileNumber = text;
        this.setState({
            mobileNumber,
        });
        if (mobileNumber && mobileNumber.length === 10) {
            if (mobileNumber.charAt(0) === "6" || mobileNumber.charAt(0) === "7" || mobileNumber.charAt(0) === "8" || mobileNumber.charAt(0) === "9")
                this.setState({ isMobileNoValid: true })
        }

    }



    handelPassword = (text) => {
        let {
            password,
        } = this.state;
        text = text.replace(/([^a-zA-Z\d ]+)/g, "")
        if (/^.*  .*$/.test(text)) {
            text = text.replace("  ", " ");
        }
        text = text.trimLeft()
        password = text;
        this.setState({
            password,
            passwordPlaceHolder: false
        });

    }





    async GetLoginUser() {
        const {
            mobileNumber,
            password
        } = this.state;

        this.setState({
            showSubmitProgressBar:true
        })
        let result = await GetLoginUser({ id: 0, mobileNumber, password })

        if (result && result.hasError === false && result.id!==0) {
            await AsyncStorage.setItem('isUserRegistered', "1")
            await AsyncStorage.setItem('userLocalStorage', JSON.stringify(result))
            await AsyncStorage.setItem('userMobileNumber', result.mobileNumber)
            this.setState({
                showSubmitProgressBar:false
            })
            this.props.navigation.navigate('Default')
        }
        else if (result && result.hasError === false  && result.id===0 ) {
            this.setState({
                logInFailed: "Mobile Number or password is incorrect.",
                showSubmitProgressBar:false
            })
        }
        
        else {
            this.setState({
                logInFailed: "Try again",
                showSubmitProgressBar:false
            })
        }
    }

    render() {
        const {
            showSubmitProgressBar,
            mobileNumberPlaceHolder,
            passwordPlaceHolder,
            toggelMobileNoSymbol,
            isMobileNoValid,
            isPasswordValid,
            toggelPasswordSymbol,
            logInFailed,
            password,
            mobileNumber
        } = this.state;


        const {
            address: lang_address,
            pincode: lang_pincode,
            cityDistrict,
            photo: lang_Photo,
            submit: lang_Submit,
            password: lang_Password,
            gender: lang_gender,
            male: lang_male,
            female: lang_female,
            dob: lang_dob
        } = language.hindi;
        return (
            <SafeAreaView>
                <ScrollView>
                    <View style={{ width: screenWidth, height: screenHeight, backgroundColor: '#FFC0CB', flex: 1, borderRadius: 5 }}>
                        <View style={{ borderRadius: 10, padding: 5, backgroundColor: 'white', flexDirection: 'column', marginVertical: 40, marginHorizontal: 10 }}>
                           

                                    <View style={{ marginTop: 25, position: 'relative' }}>
                                        <Text style={mobileNumber || mobileNumberPlaceHolder ? styles.asLabel : styles.placeholder}>{language.hindi.mobileNo}</Text>

                                        <TextInput
                                            value={mobileNumber}
                                            ref='mobileNumber'
                                            style={styles.textbox}
                                            keyboardType="numeric"
                                            maxLength={10}
                                            onChangeText={(text) => { this.handelmobileNo(text) }}
                                            returnKeyType={"next"}
                                            onSubmitEditing={(e) => { !address1 && this.refs.address1.focus() }}
                                            onFocus={() => { this.setState({ mobileNumberPlaceHolder: true }) }}
                                        //  onBlur={() => this.onBlurmobileNumber(mobileNumber)}
                                        ></TextInput>

                                        {toggelMobileNoSymbol && <Text style={isMobileNoValid ? styles.tick : styles.cross}>{isMobileNoValid ? '' : 'Fill your MobileNumber'}</Text>}
                                        {toggelMobileNoSymbol && isMobileNoValid && <Image style={isMobileNoValid ? styles.tick : styles.cross} source={require('../../cms/icons/common/Tick.png')}></Image>}

                                    </View>
                

                        

                            <View style={{ position: 'relative', marginTop: 25 }}>
                                <Text style={password || passwordPlaceHolder ? styles.asLabel : styles.placeholder}>{lang_Password}</Text>

                                <TextInput
                                    value={password}
                                    ref='city'
                                    style={styles.textbox}
                                    onChangeText={(text) => { this.handelPassword(text) }}
                                    onFocus={() => { this.setState({ passwordPlaceHolder: true }) }}

                                ></TextInput>
                                {toggelPasswordSymbol && <Text style={isPasswordValid ? styles.tick : styles.cross}>{isPasswordValid ? '' : 'Fill your password '}</Text>}
                                {toggelPasswordSymbol && isPasswordValid && <Image style={isPasswordValid ? styles.tick : styles.cross} source={require('../../cms/icons/common/Tick.png')}></Image>}

                            </View>

                            <View style={{ alignSelf: 'center', marginTop: 7, position: 'relative' }}>
                                <TouchableOpacity
                                    style={styles.submitBtn}
                                    onPress={() => { this.GetLoginUser() }}
                                >
                                    <Text style={styles.text}>{lang_Submit}</Text>
                                    <ProgressBarAndroid style={{ display: `${showSubmitProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 50, flexDirection: "row", justifyContent: 'space-around' }}>
                                <Text 
                                onPress={() => this.props.navigation.navigate("ContactUs")}
                                style={{ marginRight: 7, textDecorationLine: 'underline', color: 'blue' }}>Contact Us</Text>
                                <Text
                                    onPress={() => this.props.navigation.navigate("User")}
                                    style={{ textDecorationLine: 'underline', color: 'blue' }}>New User / Register</Text>
                            </View>
                            <View><Text style={{ color: 'red', fontSize: 20, fontWeight: '700' }}>{logInFailed}</Text></View>


                        </View>
                    </View>
                </ScrollView></SafeAreaView>
        )
    }
}

const mapStatetoProps = state => {
    return {
        state
    }
}

const mapDispatchtoProps = dispatch => {
    return {
        saveUserData: data => dispatch(saveUserData(data))
    };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(Login);

const styles = StyleSheet.create({

    body: {

    },
    label: {
        fontSize: 16,
        position: 'absolute',
        top: 0
    },
    asLabel: {
        top: -10,
        fontSize: 16,
        position: 'absolute',
    },
    placeholder: {
        bottom: 0,
        fontSize: 16,

        position: 'absolute',
        // fontWeight:''
    },
    tick: {
        position: 'absolute',
        right: 40,
        fontSize: 24,
        top: -10,
        color: 'green'
    },
    horizontalTick: {
        position: 'absolute',
        right: 40,
        fontSize: 24,
        right: -1,
        color: 'green'
    },
    cross: {
        position: 'absolute',
        right: 40,
        fontSize: 18,
        top: -10,
        color: 'red'
    },

    textbox: {
        borderRadius: 8,
        //    borderWidth: 2,
        borderBottomWidth: 2,
        // width: 380,
        height: 40,
        fontSize: 16,
        paddingBottom: 0,


    },
    textboxSmall: {
        borderRadius: 8,
        // borderWidth: 2,
        borderBottomWidth: 2,
        width: 190,
        height: 47,
        fontSize: 16,
        paddingBottom: 0,

    },
    submitBtn: {
        width: screenWidth * 0.90,
        height: 20,
        backgroundColor: 'green',
        borderColor: 'green',
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30,
        flexDirection: 'row'
    },
    text: {
        color: 'white',
        fontSize: 16
    },
    imageIconStyle: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
    },
});

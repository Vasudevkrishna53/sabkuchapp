import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Image
} from 'react-native';
import { getApps } from '../Service/Kaamkaaj';
import { Apps } from '../Common/HelperData'
import language from '../cms/lang/DefaultScreenLanguage';
import Transport from './Transport/Transport';
import Hotel from './Realstate/Realstate';

class Default extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apps: []

        }
    }
    async componentDidMount() {
        const {
            state
        } = this.props;
        const {
            apps
        } = state;

        if (apps && apps.length > 0) {
            this.setState({ apps })
        }
        else {
            let result = await getApps();
            if (result) {
                result.sort(function (a, b) {
                    if (a.zorder < b.zorder) {
                        return -1;
                    }

                    else {
                        return 0;
                    }
                })
                this.setState({ apps: result })
            }
        }

    }

    navigateTo = (id) => {

        if (id === "3") {
            this.props.navigation.navigate('Work')
        }

        else if (id === "1") {
            this.props.navigation.navigate('Shopping')
        }



        else if (id === "2") {
            this.props.navigation.navigate('Transport')
        }

        else if (id === "5") {
            this.props.navigation.navigate('Hotle')
        }

        else if (id === "6") {
            this.props.navigation.navigate('Kirana')
        }

        else if (id === "7") {
            this.props.navigation.navigate('Realstate')
        }

        else if (id === "8") {
            this.props.navigation.navigate('VegitableFruit')
        }


    }

    getImagePath = (id) => {

        if (id === "3") {
            return require('../cms/icons/Default/Worker.png')
        }

        else if (id === "1") {
            return require('../cms/icons/Default/Transport.jpg')
        }



        else if (id === "2") {
            return require('../cms/icons/Default/Transport.jpg')
        }

        else if (id === "5") {
            return require('../cms/icons/Default/Hotel.jpg')
        }

        else if (id === "6") {
            return require('../cms/icons/Default/Kirana.jpg')
        }

        else if (id === "7") {
            return require('../cms/icons/Default/Transport.jpg')
        }

        else if (id === "8") {
            return require('../cms/icons/Default/VegFruit.jpg')
        }

    }

    getAppName = (id)=>{

        const {
            Work:lang_Work,
            Tranport: lang_Transport,
            Hotel: lang_Hotel,
            Kirana: lang_Kirana,
            FruitVegitable: lang_FruitVegitable,
            off: lang_off

        } = language.hindi


        if (id === "3") {
            return lang_Work;
        }

        else if (id === "1") {
            return lang_Transport;
        }



        else if (id === "2") {
            return lang_Transport;
        }

        else if (id === "5") {
            return lang_Hotel;
        }

        else if (id === "6") {
            return lang_Kirana
        }

        else if (id === "7") {
            return lang_Transport
        }

        else if (id === "8") {
            return lang_FruitVegitable
        }
    }

    replaceoff=(str)=>{
        return str.replace("off", language.hindi.off);
    }

    render() {
        const {
            apps
        } = this.state;
        return (
            <View style={styles.body}>
                <View style={{ marginHorizontal: 25, marginVertical: 25, flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between', }}>

                    {
                        apps && apps.map(app => {
                            const {
                                id,
                                name,
                                isActive,
                                description1,
                                description2
                            } = app;
                            return (
                                <View style={{ margin: 10, position: 'relative' }}>
                                    <TouchableOpacity style={styles.appChips}
                                        onPress={() => { this.navigateTo(id) }}
                                    >
                                        <Image style={{width:90}} source={this.getImagePath(id)}></Image>
                                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{this.getAppName(id)}</Text>
                                        <Text style={{ fontSize: 14, fontWeight: 'bold' ,color:'green'}}>{this.replaceoff(description2)}</Text>
                                    </TouchableOpacity>

                                </View>
                            )
                        })
                    }

                    {/* <View>
                        <TouchableOpacity style={styles.searchWorkBtn}
                            onPress={() => { this.props.navigation.navigate('UnPickedWork') }}
                        >
                            <Text style={styles.text}>{language.hindi.searchWork}</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>
            </View>
        )
    }
}


const mapStatetoProps = state => {
    // console.log('value from store is ', state)
    return {
        state
    }
}

export default connect(mapStatetoProps, null)(Default);


const styles = StyleSheet.create({

    body: {
        flex: 1,
        backgroundColor: 'pink'

    },
    label: {
        fontSize: 24
    },

    appChips: {
        width: 140,
        height: 140,
        backgroundColor: 'white',
        borderColor: 'white',
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    searchWorkBtn: {
        width: 120,
        height: 120,
        backgroundColor: '#AB0808',
        borderColor: 'pink',

        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'black',
        fontSize: 18,
        flexWrap: 'nowrap'
    }
});

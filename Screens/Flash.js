import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  TextInput,
  ProgressBarAndroid,
  Platform,
  Image,
  PermissionsAndroid,
  Dimensions,
  TouchableHighlight,
  AsyncStorage,
  Animated,
  Easing

} from 'react-native';
import { getApps } from '../Service/Kaamkaaj';
import { saveAppsData } from '../store/reducer/kaamKaajActiion';

const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;

class Flash extends Component {
  constructor(props) {
    super(props);
    this.RotateValueHolder = new Animated.Value(0);
    this.state = {
      apps: []
    }
  }

  async componentDidMount() {
    const {
      saveAppsData
    } = this.props;

    this.StartImageRotateFunction();

    

    const isUserRegeisterd = await AsyncStorage.getItem('isUserRegistered')

    

    if (isUserRegeisterd === "1") {

      let result = await getApps();
  
      if (result) {
        
        result.sort(function (a, b) {
          if (a.zorder < b.zorder) {
            return -1;
          }

          else { 
            return 0;
          }
        })

        saveAppsData(result);

        this.setState({ apps: result })
        this.props.navigation.navigate('Default')
        

      }


      
    }
    else {

      this.props.navigation.navigate('User')
      
    }



  }
  StartImageRotateFunction() {
    this.RotateValueHolder.setValue(0);
    Animated.timing(this.RotateValueHolder, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear,
      useNativeDriver: true
    }).start(() => this.StartImageRotateFunction());
  }


  render() {
    const RotateData = this.RotateValueHolder.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    return (<View style={styles.body}>

      <Animated.Image
        style={{
          width: 150,
          height: 150,
          transform: [{ rotate: RotateData }],
        }}
        source={require('../cms/icons/common/Logo.png')}

        // source={{ uri: 'https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/icons/Logo.png' }}
      />


      <Text style={{ fontSize: 32, color: 'red', fontWeight: 'bold' }}>SAB KUCH</Text>
    </View>)
  }
}

const styles = StyleSheet.create({
  body: {
    width: screenWidth,
    height: screenHeight,
    backgroundColor: '#FFC0CB',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

const mapStatetoProps = state => {
  
  return {
    user: state.user
  }
}

const mapDispatchtoProps = dispatch => {
  return {
    saveAppsData: data => dispatch(saveAppsData(data))
  };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(Flash)
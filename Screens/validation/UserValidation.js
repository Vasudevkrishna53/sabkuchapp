export const isUserNameValid = (name) => {
    let isValid = true
    if (name) {
        if (name.length < 2) {
            isValid = false
        }
        //    else if(/^[a-zA-Z\s]{1,}$/.test(name)){
        //         isValid=false;
        //     }
        else {
            isValid = true;
        }

    }
    else {
        isValid = false;

    }
    return isValid;
}

export const isMobileNumberValid = (mobileNumber) => {
    let isValid = true
    if (mobileNumber && mobileNumber.length === 10) {
        if (mobileNumber.charAt(0) === "6" || mobileNumber.charAt(0) === "7" || mobileNumber.charAt(0) === "8" || mobileNumber.charAt(0) === "9") {
            isValid = true;
        }
        else {
            isValid = false;
        }
    }
    else {
        isValid = false;

    }
    return isValid;
}

export const isUserAddress1Valid = (address1) => {
    let isValid = true
    if (address1) {
        if (address1.length < 10) {
            isValid = false;
        }
        else {
            isValid = true;
        }
    }
    else {
        isValid = false;

    }
    return isValid;
}

export const isUserPincodeValid = (pincode) => {
    let isValid = true
    if (pincode) {
        if (pincode.length < 6) {
            isValid = false;
        }
        else {
            isValid = true;
        }
    }
    else {
        isValid = false;

    }
    return isValid;
}

export const isUserCityValid = (city) => {
    let isValid = true
    if (city) {
        if (city.length < 3) {
            isValid = false;
        }
        else {
            isValid = true;
        }
    }
    else {
        isValid = false;

    }
    return isValid;
}



export const isUserDobValid = (dob) => {
    let isValid = true
    if (dob) {
        if (dob.length < 1) {
            isValid = false;
        }
        else {
            isValid = true;
        }
    }
    else {
        isValid = false;

    }
    return isValid;
}

export const isUserGenderValid = (gender) => {
    let isValid = true
    if (gender) {
        if (gender.length < 1) {
            isValid = false;
        }
        else {
            isValid = true;
        }
    }
    else {
        isValid = false;

    }
    return isValid;
}

export const isUserPasswordValid = (password) => {
    let isValid = true
    if (password) {
        if (password.length < 1) {
            isValid = false;
        }
        else {
            isValid = true;
        }
    }
    else {
        isValid = false;

    }
    return isValid;
}



export const isUserDataValid = (user) => {
    const {
        name,
        mobileNumber,
        address,
        dob,
        gender,
        password
    } = user
    const {
        address1,
        pincode,
        city
    } = address
    let isValid = true;
    if (isUserNameValid(name) 
    && isMobileNumberValid(mobileNumber) 
    && isUserAddress1Valid(address1) 
    && isUserPincodeValid(pincode) 
    && isUserCityValid(city)
    && isUserDobValid(dob)
    && isUserGenderValid(gender)
    && isUserPasswordValid(password)
    ) {
        isValid = true;
    }
    else {
        isValid = false;
    }
    return isValid;
}
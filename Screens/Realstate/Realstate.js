import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid,
    Dimensions
} from 'react-native';

import SideBar from '../../Component/SideBar';

import Header from '../../Component/Header';

import language from '../../cms/lang/language';

import { getGetHotelList, createUpdateWork } from '../../Service/Kaamkaaj';

import { TouchableHighlight } from 'react-native-gesture-handler';
import { SliderBox } from "react-native-image-slider-box";
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen')



export default class Hotel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showWorkModal: false,
            shoppingList: [],
            workMaster: [],
            isSideBarDisplayed: false,
            user: {
                id: "288179d0-d2b8-4d6d-9b2a-d8e1bd8d6860"
            },
            selectedItem: [],
            showCurrentAddress: true,
            showEditAddress: false,
            address1: '',
            address2: '',
            pincode: '',
            state: '',
            selectedValue: 1,
            showItemNumber: false,
            num: [],
            showSearchingCard: false,
            showBookedWorkerCard: false
        }
    }

    async componentDidMount() {
        const {
            num
        } = this.state;
        const result = await getGetHotelList();


        result && result.map(x => {
            x.quantity = 1;
        })
        if (result && result.length > 0) {
            result.sort(function (x, y) {
                if (x.zOrder < y.zOrder) {
                    return -1;
                }
                else {
                    return 1;
                }
            })
            this.setState({
                shoppingList: result
            })
        }

        let i = 1;
        for (i; i < 20; i++) {
            num.push(i)
        }
        this.setState({ num })
    }

    async createUpdateWork() {

        let result = "";

        const {
            user,
            selectedItem
        } = this.state
        let response = await createUpdateWork({ user, selectedItem })

        if (response) {
            return response;
        }
        else {
            return result;
        }
    }

    handelWorkPress = async (id) => {

        const {
            workMaster,
            selectedItem,

        } = this.state;
        this.setState({
            showBookedWorkerCard: false,
            showSearchingCard: true
        })

        const index = workMaster.findIndex(x => x.id === id)
        if (index != -1) {
            workMaster[index].isActive = !workMaster[index].isActive;
            if (workMaster[index].isActive === true) {
                selectedItem.push({ workID: workMaster[index].id, name: workMaster[index].name });
            }
            else {
                if (selectedItem.length > 0) {
                    const removeIndex = selectedItem.findIndex(x => x.workID === id)
                    selectedItem.splice(removeIndex, 1);
                }
            }

        }

        let response = await this.createUpdateWork();
        if (response) {
            this.setState({
                showSearchingCard: false,
                showBookedWorkerCard: true
            })
        }
        else {
            this.setState({
                showSearchingCard: false,

            })
        }

        this.setState({
            workMaster,
            selectedItem
        });

    }

    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    handelItemQuantity = (x) => {
        const {
            selectedItem,
            workMaster,
            shoppingList
        } = this.state;
        const index = shoppingList.findIndex(x => x.id === selectedItem);
        if (index !== -1) {
            const price = shoppingList[index].price / shoppingList[index].quantity;
            shoppingList[index].price = price * x;
            shoppingList[index].quantity = x
        }
        this.setState({ showItemNumber: false, shoppingList })
    }

    render() {
        const {

            shoppingList,
            isSideBarDisplayed,
            showItemNumber,
            num,
            showSearchingCard,
            showBookedWorkerCard
        } = this.state;

        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <ScrollView style={styles.body}>


                    <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                        <View style={{ flex: 1 }}>

                            <View style={{ backgroundColor: '#84C2F9', flex: 1, flexDirection: 'column' }}>
                                <View style={{ marginVertical: 10, marginHorizontal: 5, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ color: 'white', fontSize: 24, fontWeight: 'bold' }}>30% off on Electronic Items</Text>
                                    <TouchableOpacity style={{ width: 100, height: 40, backgroundColor: '#0A1AF5', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                                        <Text style={{ color: 'white', fontSize: 20 }}>View</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ marginTop: 30, marginHorizontal: 5 }}>
                                    {
                                        shoppingList && shoppingList.map((x, index) => {
                                            const {
                                                name,
                                                id,
                                                price,
                                                quantity,
                                                photo,
                                                photo1,
                                                photo2
                                            } = x;
                                            return (
                                                <View
                                                    style={[styles.chips]}
                                                >
                                                    <View style={{ width: screenWidth / 2 }}>
                                                        <Text style={{ color: 'black', fontSize: 24, fontWeight: 'bold' }}>
                                                            {/* {language.hindi.workMaster.filter(x => x.id == id)[0].value} */}
                                                            {name} HOTEL
                                                        </Text>
                                                        <Text style={{fontFamily:''}}>
                                                            Fully AC
                                                        </Text>
                                                        <Text>
                                                            Meals and BreakFast
                                                        </Text>
                                                        <Text>
                                                            Zym/Spa
                                                        </Text>
                                                        <Text>
                                                            0.5Km away Airport
                                                        </Text>
                                                        <Text>
                                                        0.5Km away Railway Station
                                                        </Text>

                                                        <TouchableOpacity
                                                            onPress={() => { this.props.navigation.navigate('BookingConfirmation', { image: photo, workName: name, price }) }}
                                                            style={{ marginVertical:10,justifyContent: 'center', alignItems: 'center', width:screenWidth/2, height: 40, backgroundColor: '#0A1AF5', fontWeight: 'bold' }}>
                                                            <Text style={{ color: 'white', fontSize: 22 }}>{'\u20B9'}{price} BOOK NOW</Text>
                                                        </TouchableOpacity>

                                                    </View>
                                                    <View style={{ marginHorizontal: 5 }}>

                                                        <SliderBox
                                                            images={[photo, photo1, photo2]}
                                                            onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                                                            sliderBoxHeight={195}
                                                            parentWidth={195}

                                                            dotColor="#FFEE58"
                                                            inactiveDotColor="white"
                                                            autoplay
                                                          
                                                            dotStyle={{
                                                                width: 15,
                                                                height: 15,
                                                                borderRadius: 15,
                                                                marginHorizontal: 10,
                                                                padding: 0,
                                                                margin: 0
                                                            }}

                                                        />
                                                    </View>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>



                            <View style={{ display: `${showSearchingCard ? 'flex' : 'none'}`, backgroundColor: 'white', marginTop: 10, borderRadius: 8 }}>
                                <View style={{ marginHorizontal: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Wait while we search</Text>
                                    <ProgressBarAndroid style={{ height: 30, width: '100%' }} styleAttr="Horizontal" color="#2196F3" />
                                </View>
                            </View>
                            <View style={{ display: `${showBookedWorkerCard ? 'flex' : 'none'}`, marginTop: 10, backgroundColor: 'white', borderRadius: 8 }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text>Worker Details</Text>
                                </View>

                                <View style={{ flexDirection: 'row', marginHorizontal: 5 }}>
                                    <View style={{ justifyContent: 'center', marginRight: 10 }}>
                                        <Image style={{ width: 65, height: 65, borderRadius: 32.5, borderWidth: 1, borderColor: 'grey' }} source={{ uri: 'https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/SidebarNavigationIcon.png' }} />
                                    </View>

                                    <View style={{ marginLeft: 10 }}>
                                        <Text style={{ fontSize: 20 }}>Worker Name</Text>
                                        <Text style={{ fontSize: 20 }}>9910906355</Text>
                                    </View>
                                </View>
                            </View>


                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    body: {
        marginTop: 40,

    },
    chips: {

        // borderWidth: 1,
        // borderColor: '#f0f0f0',
        borderRadius: 8,
         backgroundColor: 'white',
        // width: 195,
        // height: 268,
        marginVertical:5,

        flexDirection: 'row'

    },
    example: {
        marginVertical: 0,
    },
    activeChips: {
        backgroundColor: 'green'
    },
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },
    card: {

        // backgroundColor: 'white',
        // borderRadius: 8,
        // height: 300,
        // marginBottom: 15,
        // overflow: 'scroll',


    },
    label: {
        fontSize: 24
    },
    textbox: {
        borderRadius: 18,
        borderWidth: 1,
        width: '100%',
        height: 50,
        fontSize: 24
    },
    submitBtn: {
        width: 164,
        height: 46,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'red',
        fontSize: 24
    }
});

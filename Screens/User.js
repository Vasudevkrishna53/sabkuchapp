import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    ProgressBarAndroid,
    Platform,
    Image,
    PermissionsAndroid,
    Dimensions,
    TouchableHighlight,
    AsyncStorage

} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import moment, { lang } from 'moment';
import RazorpayCheckout from 'react-native-razorpay';
import {
    createUpdateUser,
    getNewOrderID
} from '../Service/Kaamkaaj';
import { saveUserData } from '../store/reducer/kaamKaajActiion'
import {
    isUserNameValid,
    isMobileNumberValid,
    isUserAddress1Valid,
    isUserPincodeValid,
    isUserCityValid,
    isUserDataValid,
    isUserPasswordValid
} from './validation/UserValidation';
import language from '../cms/lang/language'
// import firebase from 'react-native-firebase';



var PushNotification = require("react-native-push-notification");

const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                id: 0,
                name: null,
                gender: null,
                dob: null,
                address: {
                    address1: null,
                    address2: null,
                    address3: null,
                    pincode: null,
                    city: null,
                    district: null,
                    state: null
                },
                mobileNumber: null,
                userType: null,
                image: null,
                imageName: null,
                imageUrl: null,
                hasError: false,
                requestCode: 0,
                message: null
            },
            isNameValid: false,
            isDobValid: true,
            isGenderValid: true,
            toggelNameValidInvalidSymbol: false,
            isMobileNoValid: false,
            toggelMobileNoSymbol: false,
            isAddress1Valid: false,
            toggelAddress1Symbol: false,
            isPincodeValid: false,
            toggelPincodeSymbol: false,
            isCityValid: false,
            isPasswordValid: false,
            toggelCitySymbol: false,
            toggelPasswordSymbol: false,
            showSubmitProgressBar: false,
            singleFile: {},
            namePlaceHolder: false,
            address1PlaceHolder: false,
            address2PlaceHolder: false,
            pincodePlaceHolder: false,
            cityPlaceHolder: false,
            passwordPlaceHolder: false,
            orderResponse: 'waiting resfonse',
            apiResponse: '',
            isUserFormValid: false,
            isCalenderDisplayed: false,
            isUserRequestOK: false

        }

        PushNotification.configure({
            onRegister: function (token) {
                // console.log("TOKEN: is ----", token);
            },
            onNotification: function (notification) {
                // console.log("NOTIFICATION:", notification);
                // notification.finish(PushNotificationIOS.FetchResult.NoData);
            },

            permissions: {
                alert: true,
                badge: true,
                sound: true,
                vibrate: true,
                vibration: 3000,
                actions: '["Yes", "No"]'
            },
            popInitialNotification: true,
            requestPermissions: true,
        });

        //  this.getFireBaseToken();

    }








    componentDidMount() {
        //  const channel = new firebase.notifications.Android.Channel('insider', 'insider channel', firebase.notifications.Android.Importance.High)
        //  firebase.notifications().android.createChannel(channel);
        // this.checkFcmPremissin();
        // this.createNotificationListner();
    }
    testPush = () => {

        PushNotification.localNotification({
            title: "My Notification Title", // (optional)
            message: "My Notification Message", // (required)
        });
    }

    handelmobileNo = (text) => {
        const {
            user
        } = this.state;

        text = text.replace(/([^\d]+)/g, "")

        user.mobileNumber = text;
        this.setState({
            user,

            mobileNumberPlaceHolder: false
        });
        if (user && user.mobileNumber.length === 10) {
            if (user.mobileNumber.charAt(0) === "6" || user.mobileNumber.charAt(0) === "7" || user.mobileNumber.charAt(0) === "8" || user.mobileNumber.charAt(0) === "9")
                this.setState({ isMobileNoValid: true })
        }

    }
    onBlurmobileNumber = (mobileNo) => {
        if (mobileNo) {
            this.setState({ toggelMobileNoSymbol: true })
            if (isMobileNumberValid(mobileNo)) {
                this.setState({ isMobileNoValid: true })
            }
            else {
                this.setState({ isMobileNoValid: false })
            }
        }
        else {
            this.setState({ toggelMobileNoSymbol: true })
            this.setState({ isMobileNoValid: false })
        }


    }

    handelName = (text) => {
        const {
            user,
        } = this.state;
        // text = text.replace(/([^a-zA-Z ]+)/g, "")
        if (/^.*  .*$/.test(text)) {
            text = text.replace("  ", " ");
        }
        text = text.trimLeft()
        user.name = text;
        // if (text.length > 2) {
        //     this.setState({ isNameValid: true })
        // }
        this.setState({
            user,
            namePlaceHolder: false
        });

    }
    onBlurUserName = (name) => {
        this.setState({ toggelNameValidInvalidSymbol: true })
        if (isUserNameValid(name)) {
            this.setState({ isNameValid: true })
        }
        else {
            this.setState({ isNameValid: false })
        }
    }
    handelAddress = (text) => {
        const {
            user
        } = this.state;

        // text = text.replace(/([^a-zA-Z\d /.,'"-;:_|]+)/g, "")
        if (/^.*  .*$/.test(text)) {
            text = text.replace("  ", " ");
        }
        text = text.trimLeft()
        user.address.address1 = text;
        if (text.length > 9) {
            this.setState({ isAddress1Valid: true })
        }
        this.setState({
            user,
            address1PlaceHolder: false
        });

    }

    handelAddress2 = (text) => {
        const {
            user
        } = this.state;

        //  text = text.replace(/([^a-zA-Z\d /.,'"-;:_|]+)/g, "")
        if (/^.*  .*$/.test(text)) {
            text = text.replace("  ", " ");
        }
        text = text.trimLeft();
        user.address.address2 = text;
        // if (text.length > 9) {
        //     this.setState({ isAddress1Valid: true })
        // }
        this.setState({
            user,
            address2PlaceHolder: false
        });

    }

    onBlurAddress = (address1) => {
        if (address1) {
            this.setState({ toggelAddress1Symbol: true })
            if (isUserAddress1Valid(address1)) {
                this.setState({ isAddress1Valid: true })
            }
            else {
                this.setState({ isAddress1Valid: false })
            }
        }
        else {
            this.setState({ toggelAddress1Symbol: true })
            this.setState({ isAddress1Valid: false })
        }


    }

    handelPincode = (text) => {
        const {
            user
        } = this.state;
        text = text.replace(/([^\d]+)/g, "")
        text = text.trimLeft()
        user.address.pincode = text;
        if (text.length === 6) {
            this.setState({ isPincodeValid: true })
        }

        this.setState({
            user,
            pincodePlaceHolder: false
        });

    }

    onBlurPincode = (pincode) => {
        if (pincode) {
            this.setState({ toggelPincodeSymbol: true })
            if (isUserPincodeValid(pincode)) {
                this.setState({ isPincodeValid: true })
            }
            else {
                this.setState({ isPincodeValid: false })
            }
        }
        else {
            this.setState({ toggelPincodeSymbol: true })
            this.setState({ isPincodeValid: false })
        }


    }

    handelCity = (text) => {
        const {
            user,
            isCityValid
        } = this.state;
        // text = text.replace(/([^a-zA-Z\d /.,'"-;:_|]+)/g, "")
        if (/^.*  .*$/.test(text)) {
            text = text.replace("  ", " ");
        }
        text = text.trimLeft()
        user.address.city = text;
        if (text.length > 2) {
            this.setState({ isCityValid: true, })
        }
        if (!user.address.address2) {
            user.address.address2 = " ";//temporary code 
        }
        this.setState({
            user,
            cityPlaceHolder: false
        });

    }

    handelPassword = (text) => {
        const {
            user,
        } = this.state;
        //  text = text.replace(/([^a-zA-Z\d ]+)/g, "")
        if (/^.*  .*$/.test(text)) {
            text = text.replace("  ", " ");
        }
        text = text.trimLeft()
        user.password = text;
        // if (text.length > 2) {
        //     this.setState({ isNameValid: true })
        // }
        this.setState({
            user,
            passwordPlaceHolder: false
        });

    }


    onBlurCity = (city) => {
        if (city) {
            this.setState({ toggelCitySymbol: true })
            if (isUserCityValid(city)) {
                this.setState({ isCityValid: true })
            }
            else {
                this.setState({ isCityValid: false })
            }
        }
        else {
            this.setState({ toggelCitySymbol: true })
            this.setState({ isCityValid: false })
        }


    }
    onBlurPassword = (password) => {
        if (password) {
            this.setState({ toggelPasswordSymbol: true })
            if (isUserPasswordValid(password)) {
                this.setState({ isPasswordValid: true })
            }
            else {
                this.setState({ isPasswordValid: false })
            }
        }
        else {
            this.setState({ toggelPasswordSymbol: true })
            this.setState({ isPasswordValid: false })
        }


    }

    handelDocument = async () => {
        if (Platform.OS) {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then((result) => {
                if (result) {
                    // console.log("Permission is OK go and get imei");


                } else {
                    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then((result) => {
                        if (result) {
                            // console.log("User accept  go and get imei");

                        } else {
                            // console.log("User refuse");
                        }
                    });
                }
            });

        }
    }

    async selectOneFile() {
        await this.handelDocument();
        //Opening Document Picker for selection of one file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
                //There can me more options as well
                // DocumentPicker.types.allFiles
                // DocumentPicker.types.images
                // DocumentPicker.types.plainText
                // DocumentPicker.types.audio
                // DocumentPicker.types.pdf
            });
            //Printing the log realted to the file
            // console.log('res : ' + JSON.stringify(res));
            // console.log('URI : ' + res.uri);
            // console.log('Type : ' + res.type);
            // console.log('File Name : ' + res.name);
            // console.log('File Size : ' + res.size);
            //Setting the state to show single file attributes
            if (res && res.uri) {
                RNFS.readFile(res.uri, 'base64')
                    .then(b64 => {
                        const {
                            user
                        } = this.state;
                        user.image = b64;
                        user.imageName = res.name;
                        this.setState({
                            user
                        })

                        // console.log(b64);
                    });
            }
            this.setState({ singleFile: res });
        } catch (err) {
            //Handling any exception (If any)
            if (DocumentPicker.isCancel(err)) {
                //If user canceled the document selection
                alert('Canceled from single doc picker');
            } else {
                //For Unknown Error
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }


    async createUpdateUser() {
        const {
            user,

        } = this.state;
        const {
            saveUserData
        } = this.props;
        this.setState({
            showSubmitProgressBar: true
        });
        if (this.isUserFormValid()) {

            this.setState({ isUserRequestOK: true })
            let result = await createUpdateUser(user);

            let {
                hasError,
                isOtpSent
            } = result
            if (result && hasError === false) {

                this.setState({
                    showSubmitProgressBar: false,
                    isUserRequestOK: false
                })
                user.imageUrl = result.imageUrl;

                saveUserData(result);


                await AsyncStorage.setItem('isUserRegistered', "1")

                await AsyncStorage.setItem('userLocalStorage', JSON.stringify(result))

                await AsyncStorage.setItem('userMobileNumber', result.mobileNumber)

                // await AsyncStorage.setItem('userUUID', result.id)

                this.props.navigation.navigate('Default')
            }
            else {
                this.setState({
                    showSubmitProgressBar: false,
                    apiResponse: result.message
                })
            }
        }
        else {
            this.setState({
                showSubmitProgressBar: false,
                toggelNameValidInvalidSymbol: true,
                toggelMobileNoSymbol: true,
                toggelAddress1Symbol: true,
                toggelPincodeSymbol: true,
                toggelCitySymbol: true,
                toggelPasswordSymbol: true
            });
        }
    }
    isUserFormValid = () => {
        const {
            user
        } = this.state;
        const {
            dob,
            gender
        } = user;
        let isValid = true;
        const {
        } = this.state;
        if (isUserDataValid(user)) {
            isValid = true;
        }
        else {
            isValid = false;
        }
        if (!dob || dob.length < 1 || !gender || gender.length < 1) {
            isValid = false;
            this.setState({ isDobValid: false, isGenderValid: false })
        }
        return isValid;
    }


    handelGender = (value) => {
        const {
            user
        } = this.state;

        user.gender = value;

        this.setState({
            user,
            isGenderValid: true
        });

    }

    handelDob = (event, selectedDate) => {

        if (event.type === 'set') {
            const {
                user
            } = this.state;
            user.dob = moment(selectedDate).format("DD-MMM-YYYY");
            console.log('user.dob', user.dob)
            this.setState({ isDobValid: true, isCalenderDisplayed: false, user })
        }
        else {
            this.setState({ isCalenderDisplayed: false })
        }


    };

    render() {
        const {
            showSubmitProgressBar,
            singleFile,
            user,
            namePlaceHolder,
            mobileNumberPlaceHolder,
            address1PlaceHolder,
            address2PlaceHolder,
            pincodePlaceHolder,
            cityPlaceHolder,
            passwordPlaceHolder,
            isNameValid,
            toggelNameValidInvalidSymbol,
            toggelMobileNoSymbol,
            isMobileNoValid,
            toggelAddress1Symbol,
            isAddress1Valid,
            toggelPincodeSymbol,
            isCityValid,
            isPasswordValid,
            toggelCitySymbol,
            toggelPasswordSymbol,
            isPincodeValid,
            orderResponse,
            apiResponse,
            isCalenderDisplayed,
            isDobValid,
            isGenderValid,
            isUserRequestOK
        } = this.state;
        const {
            name,
            mobileNumber,
            address,
            password, gender,
            dob
        } = user;
        const {
            address1,
            address2,
            pincode,
            city,
        } = address;
        const {
            address: lang_address,
            address2: lang_address2,
            pincode: lang_pincode,
            cityDistrict,
            photo: lang_Photo,
            submit: lang_Submit,
            password: lang_Password,
            gender: lang_gender,
            male: lang_male,
            female: lang_female,
            dob: lang_dob,
            fillDetail: lang_detail,
            fillName: lang_fillName,
            fillMobileNo: lang_fillMobileNumber,
            fillGender: lang_fillGender,
            fillDob: lang_fillDob,
            fillAddress: lang_fillAddress,
            fillPincode: lang_fillPincode,
            fillDistrict: lang_filldistrict,
            fillPassword: lang_fillPassword,
            formSubmitionText: lang_formSubmitionText,
        } = language.hindi;
        return (
            <SafeAreaView>
                <ScrollView>
                    <View style={{ width: screenWidth, height: screenHeight, backgroundColor: '#FFC0CB', flex: 1, borderRadius: 5 }}>
                        <View style={{ borderRadius: 10, padding: 5, backgroundColor: 'white', flexDirection: 'column', marginVertical: 10, marginHorizontal: 10 }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 16, color: 'red', fontWeight: 'bold' }}>{lang_detail}</Text>
                            </View>

                            <View style={{ marginTop: 25, position: 'relative' }}>
                                <Text style={name || namePlaceHolder ? styles.asLabel : styles.placeholder}>{language.hindi.name}</Text>
                                <TextInput

                                    value={name}
                                    style={styles.textbox}
                                    onChangeText={(text) => { this.handelName(text) }}
                                    onFocus={() => { this.setState({ namePlaceHolder: true }) }}
                                    onBlur={() => this.onBlurUserName(name)}
                                    returnKeyType={"next"}
                                    onSubmitEditing={(e) => { !mobileNumber && this.refs.mobileNumber.focus() }}
                                // placeholder={language.hindi.name}
                                ></TextInput>
                                {toggelNameValidInvalidSymbol && <Text style={isNameValid ? styles.tick : styles.cross}>{isNameValid ? '' : lang_fillName}</Text>}
                                {toggelNameValidInvalidSymbol && isNameValid && <Image style={isNameValid ? styles.tick : styles.cross} source={require('../cms/icons/common/Tick.png')}></Image>}

                            </View>
                            <View style={{ marginTop: 25, position: 'relative' }}>
                                <Text style={mobileNumber || mobileNumberPlaceHolder ? styles.asLabel : styles.placeholder}>{language.hindi.mobileNo}</Text>

                                <TextInput
                                    value={mobileNumber}
                                    ref='mobileNumber'
                                    style={styles.textbox}
                                    keyboardType="numeric"
                                    maxLength={10}
                                    onChangeText={(text) => { this.handelmobileNo(text) }}
                                    returnKeyType={"next"}
                                    onSubmitEditing={(e) => { !address1 && this.refs.address1.focus() }}
                                    onFocus={() => { this.setState({ mobileNumberPlaceHolder: true }) }}
                                    onBlur={() => this.onBlurmobileNumber(mobileNumber)}
                                ></TextInput>

                                {toggelMobileNoSymbol && <Text style={isMobileNoValid ? styles.tick : styles.cross}>{isMobileNoValid ? '' : lang_fillMobileNumber}</Text>}
                                {toggelMobileNoSymbol && isMobileNoValid && <Image style={isMobileNoValid ? styles.tick : styles.cross} source={require('../cms/icons/common/Tick.png')}></Image>}

                            </View>


                            {/* <View style={{ marginTop: 45, marginLeft: 20, flexDirection: 'row', justifyContent: 'space-between' }}>

                                    <TouchableOpacity
                                        style={{ position: 'relative', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', width: 100, height: 100, borderRadius: 50, borderWidth: 1, borderColor: 'grey' }}
                                        onPress={() => { this.selectOneFile() }}
                                    >
                                        <Text style={{ fontSize: 20, position: 'absolute', top: 35 }}>{lang_Photo}</Text>

                                        {singleFile && <Image style={{ width: 100, height: 100, borderRadius: 50 }} source={{ uri: singleFile.uri }} />}
                                    </TouchableOpacity>


                                </View>
                            */}


                            <View style={{ marginTop: 25, flexDirection: 'row' }}>

                                <View style={{ marginRight: 5 }}>
                                    <Text>{lang_gender}</Text>
                                    <Text style={isGenderValid ? styles.tick : styles.cross}>{isGenderValid ? '' : lang_fillGender}</Text>

                                    <View style={{ justifyContent: 'space-between', padding: 5, borderWidth: 2, borderColor: 'black', position: 'relative', flexDirection: 'row', width: 190, height: 50 }}>
                                        {/* <Text style={{ width: 190, fontSize: 12, paddingHorizontal: 10 }}>Gender</Text> */}

                                        <TouchableOpacity
                                            style={{ borderWidth: 2, borderColor: 'black', width: 80, height: 38, borderRadius: 40, justifyContent: 'center', alignItems: 'center' }}
                                            onPress={() => this.handelGender("Male")}
                                        >
                                            <Text>{lang_male}</Text>
                                            {gender === "Male" && <Image style={styles.horizontalTick} source={require('../cms/icons/common/Tick.png')}></Image>}

                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => this.handelGender("Female")}
                                            style={{ borderWidth: 2, borderColor: 'black', width: 80, height: 38, borderRadius: 40, justifyContent: 'center', alignItems: 'center' }}
                                        >
                                            <Text>{lang_female}</Text>
                                            {gender === "Female" && <Image style={styles.horizontalTick} source={require('../cms/icons/common/Tick.png')}></Image>}

                                        </TouchableOpacity>

                                    </View>
                                </View>

                                <TouchableOpacity
                                    style={{ borderColor: 'black', borderBottomWidth: 2, width: 190, marginTop: -25 }}
                                    onPress={() => { this.setState({ isCalenderDisplayed: true }) }}
                                >
                                    <View >
                                        <Text style={{ marginTop: 30, fontSize: 16 }}>{lang_dob}</Text>
                                        {
                                            isCalenderDisplayed === true && <DateTimePicker
                                                //testID="dateTimePicker"
                                                value={dob ? new Date(dob) : new Date()}
                                                mode='datetime'
                                                is24Hour={false}
                                                display='spinner'
                                                onChange={this.handelDob}
                                            />
                                        }
                                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                            <Text>{dob}</Text>
                                            {dob && dob.length > 0 && <Image style={styles.horizontalTick} source={require('../cms/icons/common/Tick.png')}></Image>}
                                            <Text style={isDobValid ? styles.tick : styles.cross}>{isDobValid ? '' : lang_fillDob}</Text>

                                        </View>




                                    </View>
                                </TouchableOpacity>

                            </View>



                            <View style={{ marginTop: 25, position: 'relative' }}>
                                <Text style={address1 || address1PlaceHolder ? styles.asLabel : styles.placeholder}>{lang_address}</Text>

                                <TextInput
                                    value={address1}
                                    ref='address1 2'
                                    style={styles.textbox}
                                    onChangeText={(text) => { this.handelAddress(text) }}
                                    onFocus={() => { this.setState({ address1PlaceHolder: true }) }}
                                    onBlur={() => { this.onBlurAddress(address1) }}
                                    onSubmitEditing={(e) => { !pincode || pincode.length < 6 && this.refs.pincode.focus() }}
                                ></TextInput>
                                {toggelAddress1Symbol && <Text style={isAddress1Valid ? styles.tick : styles.cross}>{isAddress1Valid ? '' : lang_fillAddress}</Text>}
                                {toggelAddress1Symbol && isAddress1Valid && <Image style={isAddress1Valid ? styles.tick : styles.cross} source={require('../cms/icons/common/Tick.png')}></Image>}

                            </View>

                            <View style={{ marginTop: 25, position: 'relative' }}>
                                <Text style={address2 || address2PlaceHolder ? styles.asLabel : styles.placeholder}>{lang_address2}</Text>

                                <TextInput
                                    value={address2}
                                    ref='address1 2'
                                    style={styles.textbox}
                                    onChangeText={(text) => { this.handelAddress2(text) }}
                                    onFocus={() => { this.setState({ address2PlaceHolder: true }) }}
                                    // onBlur={() => { this.onBlurAddress(address2) }}
                                    onSubmitEditing={(e) => { !pincode || pincode.length < 6 && this.refs.pincode.focus() }}
                                ></TextInput>

                            </View>


                            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                                <View style={{ position: 'relative' }}>
                                    <Text style={pincode || pincodePlaceHolder ? styles.asLabel : styles.placeholder}>{lang_pincode}</Text>

                                    <TextInput
                                        value={pincode}
                                        ref='pincode'
                                        keyboardType="numeric"
                                        maxLength={6}
                                        style={styles.textboxSmall}
                                        onChangeText={(text) => { this.handelPincode(text) }}
                                        onFocus={() => { this.setState({ pincodePlaceHolder: true }) }}
                                        onBlur={() => { this.onBlurPincode(pincode) }}
                                        onSubmitEditing={(e) => { !city && this.refs.city.focus() }}
                                    ></TextInput>
                                    {toggelPincodeSymbol && <Text style={isPincodeValid ? styles.tick : styles.cross}>{isPincodeValid ? '' : lang_fillPincode}</Text>}
                                    {toggelPincodeSymbol && isPincodeValid && <Image style={isPincodeValid ? styles.tick : styles.cross} source={require('../cms/icons/common/Tick.png')}></Image>}

                                </View>
                                <View style={{ position: 'relative' }}>
                                    <Text style={city || cityPlaceHolder ? styles.asLabel : styles.placeholder}>{cityDistrict}</Text>

                                    <TextInput
                                        value={city}
                                        ref='city'
                                        style={styles.textboxSmall}
                                        onChangeText={(text) => { this.handelCity(text) }}
                                        onFocus={() => { this.setState({ cityPlaceHolder: true }) }}
                                        onBlur={() => { this.onBlurCity(city) }}
                                    ></TextInput>
                                    {toggelCitySymbol && <Text style={isCityValid ? styles.tick : styles.cross}>{isCityValid ? '' : lang_filldistrict}</Text>}
                                    {toggelCitySymbol && isCityValid && <Image style={isCityValid ? styles.tick : styles.cross} source={require('../cms/icons/common/Tick.png')}></Image>}

                                </View>

                            </View>
                            <View style={{ position: 'relative', marginTop: 25 }}>
                                <Text style={password || passwordPlaceHolder ? styles.asLabel : styles.placeholder}>{lang_Password}</Text>

                                <TextInput
                                    value={password}
                                    ref='city'
                                    style={styles.textbox}
                                    onChangeText={(text) => { this.handelPassword(text) }}
                                    onFocus={() => { this.setState({ passwordPlaceHolder: true }) }}
                                    onBlur={() => { this.onBlurPassword(password) }}
                                ></TextInput>
                                {toggelPasswordSymbol && <Text style={isPasswordValid ? styles.tick : styles.cross}>{isPasswordValid ? '' : lang_fillPassword}</Text>}
                                {toggelPasswordSymbol && isPasswordValid && <Image style={isPasswordValid ? styles.tick : styles.cross} source={require('../cms/icons/common/Tick.png')}></Image>}

                            </View>

                            <View style={{ alignSelf: 'center', marginTop: 25, position: 'relative' }}>
                                {/* {isUserRequestOK===true && <Text  style={{alignSelf:'center ', color:'red',fontSize:16,fontWeight:'bold'}}>{lang_formSubmitionText}</Text>} */}
                                <TouchableOpacity
                                    style={styles.submitBtn}
                                    onPress={() => { this.createUpdateUser() }}
                                >
                                    <Text style={styles.text}>{lang_Submit}</Text>
                                    <ProgressBarAndroid style={{ display: `${showSubmitProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 50, flexDirection: "row", justifyContent: 'space-around' }}>
                                <Text
                                    onPress={() => this.props.navigation.navigate("ContactUs")}
                                    style={{ marginRight: 7, textDecorationLine: 'underline', color: 'blue' }}>Contact Us</Text>
                                <Text
                                    onPress={() => this.props.navigation.navigate("Login")}
                                    style={{ textDecorationLine: 'underline', color: 'blue' }}>Already a user/Log In</Text>
                            </View>

                            <View><Text style={{ color: 'red', fontSize: 20, fontWeight: '700' }}>{apiResponse}</Text></View>


                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapStatetoProps = state => {
    return {
        state
    }
}

const mapDispatchtoProps = dispatch => {
    return {
        saveUserData: data => dispatch(saveUserData(data))
    };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(User);

const styles = StyleSheet.create({

    body: {

    },
    label: {
        fontSize: 16,
        position: 'absolute',
        top: 0
    },
    asLabel: {
        top: -10,
        fontSize: 16,
        position: 'absolute',
    },
    placeholder: {
        bottom: 0,
        fontSize: 16,

        position: 'absolute',
        // fontWeight:''
    },
    tick: {
        position: 'absolute',
        right: 40,
        fontSize: 24,
        top: -10,
        color: 'green'
    },
    cross: {
        position: 'absolute',
        right: 40,
        fontSize: 18,
        top: -10,
        color: 'red'
    },

    textbox: {
        borderRadius: 8,
        //    borderWidth: 2,
        borderBottomWidth: 2,
        // width: 380,
        height: 40,
        fontSize: 16,
        paddingBottom: 0,


    },
    textboxSmall: {
        borderRadius: 8,
        // borderWidth: 2,
        borderBottomWidth: 2,
        width: 190,
        height: 47,
        fontSize: 16,
        paddingBottom: 0,

    },
    submitBtn: {
        width: screenWidth * 0.90,
        height: 20,
        backgroundColor: 'green',
        borderColor: 'green',
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30,
        flexDirection: 'row'
    },
    text: {
        color: 'white',
        fontSize: 16
    },
    imageIconStyle: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
    },
});

import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    TouchableWithoutFeedback
} from 'react-native';

import SideBar from '../Component/SideBar';
import Header from '../Component/Header';

import language from '../cms/lang/language';
import { getOwnerWorks } from '../Service/Kaamkaaj';

export default class IndividualWorkList extends Component {
    constructor(props) {
        super(props);
        this.state = {

            ownerWorks: [],
            isSideBarDisplayed: false,

        }
    }

    async componentDidMount() {
        const result = await getOwnerWorks();
        if (result && result.length > 0) {
            // result.sort(function (x, y) {
            //     if (x.zOrder < y.zOrder) {
            //         return -1;
            //     }
            //     else {
            //         return 1;
            //     }
            // })
            this.setState({
                ownerWorks: result
            })
        }
    }


    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    render() {
        const {
            ownerWorks,
            isSideBarDisplayed,
            
        } = this.state;
        return (<SafeAreaView>
            <Header
                handelMenuDisplayer={this.handelMenuDisplayer}
            />
            {isSideBarDisplayed === true && <SideBar {...this.props} />}
            <ScrollView style={styles.body}>


                <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                    <View style={{ flex: 1 }}>

                        <View style={{ flex: 1, flexDirection: 'column', marginLeft: 20 }}>

                            {ownerWorks && ownerWorks.length > 0 && ownerWorks.map(x => {
                                const{
                                    id,
                                    name,
                                    description,
                                    createdOn,
                                    address
                                }=x;
                                return (
                                    <View style={styles.card}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
                                            <View>
                                                <Text style={{ fontSize: 18 }}>Work Name</Text>
                                                <Text style={styles.label}>{name}</Text>
                                            </View>
                                            <View>
                                                <Text style={{ fontSize: 18 }}>Created On</Text>
                                                <Text style={styles.label}>{createdOn}</Text>
                                            </View>
                                        </View>

                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <View>
                                                <Text style={{ fontSize: 18 }}>Status</Text>
                                                <Text style={[styles.label, styles.successColor]}>Active</Text>
                                            </View>
                                            <View>
                                                <TouchableOpacity 
                                                onPress={()=>{
                                                    alert(id)
                                                }}
                                                style={{ justifyContent: 'center', alignItems: 'center', marginRight: 5, backgroundColor: 'red', borderRadius: 8, width: 137, height: 37 }}>
                                                    <Text style={{ color: 'white',fontSize:18 }}>Cancel</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                    </View>

                                );
                            })}

                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
        </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    body: {
        marginTop: '7%',
    },


    card: {
        backgroundColor: 'white',
        borderRadius: 24,
        marginBottom: 15,

    },
    label: {
        fontSize: 24
    },

    text: {
        color: 'white',
        fontSize: 24
    }
});

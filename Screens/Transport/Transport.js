import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    PermissionsAndroid,
    Image,
    Dimensions,
    TouchableWithoutFeedback,
    Linking,
    BackHandler,
    AsyncStorage
} from 'react-native';
import { getApps, GetTransport, CreateUpdateTransportOrder } from '../../Service/Kaamkaaj';
import { Apps } from '../../Common/HelperData'

import language from '../../cms/lang/language';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
navigator.geolocation = require('@react-native-community/geolocation');
import Footer from '../../Component/Footer';
import SideBar from '../../Component/SideBar';
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen')


class Transport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apps: [],
            latitude: null,
            longitude: null,
            transportList: [],
            smallCabQuote: {},
            mediumCabQuote: {},
            largeCabQuote: {},
            showLoadingCard: true,
            showCabCard: true,
            showDriverCard: false,
            loadingArray: [1, 2, 3],
            isLocationPermissionGranted: false,
            isSideBarDisplayed: false,
            transportRequest: {}

        }
    }
    async componentDidMount() {
        const {
            state
        } = this.props;
        let {

            smallCabQuote,
            mediumCabQuote,
            largeCabQuote,
            showLoadingCard,
            showSearchingCard,
            showCabCard,
            showDriverCard,

        } = state;


        if (Platform.OS) {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                if (result) {

                    navigator.geolocation.getCurrentPosition(
                        (position) => {

                            this.setState({
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                                error: null,
                                isLocationPermissionGranted: true,
                                isSideBarDisplayed: false
                            });
                        },
                        (error) => this.setState({ error: error.message }),
                        { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
                    );



                } else {
                    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
                        if (result) {

                            // console.log('value of result is ', result)
                            // console.log(navigator.geolocation.getCurrentPosition())
                            navigator.geolocation.getCurrentPosition(
                                (position) => {
                                    // console.log("wokeeey");
                                    // console.log(position);
                                    this.setState({
                                        latitude: position.coords.latitude,
                                        longitude: position.coords.longitude,
                                        error: null,
                                        isLocationPermissionGranted: true,
                                        isSideBarDisplayed: false
                                    });
                                },
                                (error) => this.setState({ error: error.message }),
                                { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
                            );

                        }
                        else {
                            this.setState({
                                isLocationPermissionGranted: false,
                                isSideBarDisplayed: false
                            })
                            console.log("User refuse");
                        }
                    });
                }
            });

        }

        let response = await GetTransport();

        if (response && !response.hasError) {

            let smallCab = response.transports.filter(x => x.type === "a" && x.subType === "a")

            let mediumCab = response.transports.filter(x => x.type === "a" && x.subType === "b")
            let largeCab = response.transports.filter(x => x.type === "a" && x.subType === "c")
            if (smallCab && smallCab.length > 0) {
                smallCabQuote = smallCab[0];
            }
            if (mediumCab && mediumCab.length > 0) {
                mediumCabQuote = mediumCab[0];
            }
            if (largeCab && largeCab.length > 0) {
                largeCabQuote = largeCab[0];
            }
            this.setState({
                smallCabQuote, mediumCabQuote, largeCabQuote,
                transportList: response.transports, showLoadingCard: false
            })
        }
    }

    backAction = () => {

        this.props.navigation.navigate('Default')
    };

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction());
    }

    onRegionChange = () => {


        navigator.geolocation.getCurrentPosition(
            (position) => {
                //  console.log('Lat is ', position.coords.latitude);

                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
        // console.log
    }

    dialCall = (number) => {
        let phoneNumber = '';
        if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
        else { phoneNumber = `telprompt:${number}`; }
        Linking.openURL(phoneNumber);
    };

    getTransport = async (type, subType) => {
        const {
            transportList
        } = this.state;


        this.setState({ showCabCard: false, showDriverCard: true, showLoadingCard: true })


        let response = await GetTransport();

        if (response && !response.hasError) {
            let selectedQuote = response.transports.filter(x => x.type === type && x.subType === subType)[0]
            this.setState({
                transportList: response.transports,
                selectedQuote, showLoadingCard: false
            }, () => this.orderNow())
        }
    }

    orderNow = async () => {
        const {
            userTransportList,
            userTotalPrice,
            showSubmitProgressBar,
            transportRequest,
            selectedQuote
        } = this.state;

        const {
            basePrice,
            name,
            vehicleNo,
            type,
            subType,
            id: vehickeID,
            name: vehicleName,
            unit,

        } = selectedQuote

        this.setState({ showSubmitProgressBar: true })

        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);


            transportRequest.userTransportItems = [{
                MobileNo: user.mobileNumber, UserId: user.id, ItemId: vehickeID, ItemName: `${type}-${subType}-${vehicleName}-${vehicleNo}`
                , ItemUnit: unit, ItemPrice: basePrice, ItemQuantity: 1, IsActive: 1, Zorder: 1
            }]

            transportRequest.userTransportOrders =
                { OrderId: 0, TotalPrice: basePrice, UserId: user.id, IsActive: 1, PaymentStatus: 1, DeliveryStatus: 1, CustomerRemarks: "", AgentRemarks: "", Address: `${user.address.address1} ${user.address.address2} ${user.address.city} ${user.address.pincode} `, UserMobileNo: user.mobileNumber, Items: `${type}-${subType}-${vehicleName}-${vehicleNo}` }

        }

        console.log('trans req----', transportRequest)
        let response = await CreateUpdateTransportOrder(transportRequest);
        console.log('trans req----', response)



    }

    getUnitLang = (val) => {
        if (val.toLowerCase() === "person") {
            return val.replace(val, language.hindi.person);
        }
        else {
            return val
        }


    }


    renderDriverCard = () => {
        const {
            showLoadingCard,
            loadingArray,
            smallCabQuote,
            mediumCabQuote,
            largeCabQuote,
            selectedQuote
        } = this.state;
        const {
            cancelBooking: lang_cancelBooking,
            callDriver: lang_CallDriver,
            tolltax:lang_tollTax,
            parking:lang_parking,
            andDispute:lang_anyDispute,
            stateGovtRule:lang_stateGovtRule,
            baseFare:lang_baseFare,
            rideWillbe:lang_ridewillbe,
            km:lang_km,
            note:lang_Note
        } = language.hindi

        return (
            !showLoadingCard ?
                <View style={{ backgroundColor: 'white', borderRadius: 8, padding: 5 }}>

                    <View style={{ paddingBottom: 1, justifyContent: 'space-between' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'row', }}>
                                <Image style={{ width: 80, height: 80, borderRadius: 40, backgroundColor: '#d3d3d3' }}
                                    source={{ uri: 'dkfs' }}
                                ></Image>
                                <Text style={{ alignSelf: 'center', paddingLeft: 7 }}>{selectedQuote.ownerName}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16 }}>{`${selectedQuote.name} ${selectedQuote.vehicleNo}`}</Text>

                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                            <View >
                                <TouchableOpacity
                                    onPress={() => { this.dialCall(selectedQuote.driverNo) }}
                                    style={{ backgroundColor: 'green', padding: 10, borderRadius: 7, justifyContent: "center", alignItems: 'center' }}
                                >
                                    <Text style={{ color: 'white', fontSize: 14 }}>{lang_CallDriver}</Text>
                                </TouchableOpacity>

                            </View>
                            <View>
                                <TouchableOpacity
                                    onPress={() => { this.setState({ showCabCard: true, showDriverCard: false, showLoadingCard: false }) }}
                                    style={{ backgroundColor: 'red', padding: 10, borderRadius: 7, justifyContent: "center", alignItems: 'center' }}
                                >
                                    <Text style={{ color: 'white', fontSize: 14 }}>{lang_cancelBooking}</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>

                    <View style={{ borderTopWidth: 1, borderColor: '#d3d3d3', justifyContent: 'space-around', padding: 5, flexWrap: 'nowrap' }}>
                        <Text style={{ alignSelf: 'stretch', fontWeight: '700', fontSize: 16 }}>{lang_Note}</Text>
                        <Text style={{ flexWrap: 'nowrap' }}>{`\u2605`} {lang_ridewillbe} {`\u20b9${selectedQuote && selectedQuote.price}/${lang_km}`} </Text>
                        <Text style={{ flexWrap: 'nowrap' }}>{`\u2605`} {`\u20b9${selectedQuote && selectedQuote.basePrice}`} {lang_baseFare}</Text>
                        <Text style={{ flexWrap: 'nowrap' }}>{`\u2605`} {lang_tollTax}</Text>
                        <Text style={{ flexWrap: 'nowrap' }}>{`\u2605`} {lang_parking} </Text>
                        <Text style={{ flexWrap: 'nowrap' }}>{`\u2605`} {lang_anyDispute}</Text>
                        <Text style={{ flexWrap: 'nowrap' }}>{`\u2605`} {lang_stateGovtRule}</Text>

                    </View>

                </View> :
                <View style={{ backgroundColor: 'white', borderRadius: 8, padding: 5 }}>
                    <View >
                        <View style={{ borderRightWidth: 1, borderRightColor: '#d3d3d3', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                <View style={{ width: 80, height: 80, backgroundColor: '#d3d3d3' }}>

                                </View>
                                <Text style={{ alignSelf: 'center' }}>Driver Name</Text>
                            </View>
                            <View style={{ width: 100, height: 10, backgroundColor: '#d3d3d3' }}>


                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ width: 75, height: 25, backgroundColor: '#d3d3d3' }}>


                                </View>
                                <View style={{ width: 75, height: 25, backgroundColor: '#d3d3d3' }}>


                                </View>
                            </View>
                        </View>

                        <View style={{ justifyContent: 'space-around', padding: 5, flexWrap: 'wrap' }}>
                            <View style={{ height: 10, width: 100, backgroundColor: '#d3d3d3' }}></View>
                            <View style={{ height: 10, width: 100, backgroundColor: '#d3d3d3' }}></View>
                            <View style={{ height: 10, width: 100, backgroundColor: '#d3d3d3' }}></View>
                            <View style={{ height: 10, width: 100, backgroundColor: '#d3d3d3' }}></View>
                            <View style={{ height: 10, width: 100, backgroundColor: '#d3d3d3' }}></View>
                            <View style={{ height: 10, width: 100, backgroundColor: '#d3d3d3' }}></View>

                        </View>
                    </View>
                </View>
        )
    }

    renderCabsCard = () => {
        const {
            showLoadingCard,
            loadingArray,
            smallCabQuote,
            mediumCabQuote,
            largeCabQuote
        } = this.state;

        const {
            bookNow: lang_bookNow,
            seaterCar: lang_seaterCar
        } = language.hindi

        return (
            !showLoadingCard ?
                <View style={{ borderRadius: 8, backgroundColor: 'white', flexDirection: 'row', flex: 1, justifyContent: 'space-around', alignItems: 'stretch' }}>
                    {smallCabQuote && smallCabQuote !== undefined && smallCabQuote !== null &&
                        <View style={{ width: '33%', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Text style={styles.label}>
                                    {`\u20B9${smallCabQuote && smallCabQuote.price}/Km`}
                                </Text>
                                <Text style={{ fontSize: 12, textDecorationLine: 'line-through' }}>
                                    {`\u20B9${smallCabQuote && (smallCabQuote.price + 6)}/Km`}
                                </Text>
                            </View>
                            <Image style={{ marginHorizontal: 10, width: 80, height: 50, borderRadius: 40 }}
                                source={{ uri: `https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/icons/Transport/SmallCar.png` }}
                            />
                            <View style={{ marginTop: -5 }}>
                                <Text>4 {lang_seaterCar}</Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => { this.getTransport("a", "a") }}
                                style={{ padding: 10, height: 40, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 18 }}>
                                    {lang_bookNow}
                                </Text>
                            </TouchableOpacity>
                        </View>



                    }
                    {mediumCabQuote && mediumCabQuote !== undefined && mediumCabQuote !== null &&
                        <View style={{ width: '33%', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Text style={styles.label}>
                                    {`\u20B9${mediumCabQuote && mediumCabQuote.price}/Km`}
                                </Text>
                                <Text style={{ fontSize: 12, textDecorationLine: 'line-through' }}>
                                    {`\u20B9${mediumCabQuote && (mediumCabQuote.price + 6)}/Km`}
                                </Text>
                            </View>

                            <Image style={{ marginHorizontal: 10, width: 80, height: 30, borderRadius: 40, marginTop: 8 }}
                                source={{ uri: `https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/icons/Transport/MediumCar.png` }}
                            />
                            <View>
                                <Text>5 Seater Car</Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => { this.getTransport("a", "b") }}
                                style={{ padding: 10, height: 40, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', marginTop: 12 }}
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}>
                                    Book Now
                    </Text>
                            </TouchableOpacity>
                        </View>
                    }

                    {largeCabQuote && largeCabQuote !== undefined && largeCabQuote !== null &&
                        <View style={{ width: '33%', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Text style={styles.label}>
                                    {`\u20B9${largeCabQuote && largeCabQuote.price}/Km`}
                                </Text>
                                <Text style={{ fontSize: 12, textDecorationLine: 'line-through' }}>
                                    {`\u20B9${largeCabQuote && (largeCabQuote.price + 6)}/Km`}
                                </Text>
                            </View>

                            <Image style={{ marginHorizontal: 10, width: 80, height: 50, borderRadius: 40 }}
                                source={{ uri: `https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/icons/Transport/BigCar.png` }}
                            />
                            <View style={{ marginTop: -5 }}>
                                <Text>7 Seater Car</Text>
                            </View>
                            <TouchableOpacity
                                style={{ padding: 10, height: 40, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center' }}
                                // onPress={() => { this.dialCall("8851057104") }}
                                onPress={() => { this.getTransport("a", "c") }}
                            >
                                <Text style={{ color: 'white', fontSize: 18 }}>
                                    Book Now
        </Text>
                            </TouchableOpacity>
                        </View>
                    }
                    {smallCabQuote === undefined && mediumCabQuote === undefined && largeCabQuote === undefined
                        && <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ marginBottom: 10 }}>Sory No Cab is available now,Try after some time</Text>
                            <Text>Or call us at 8851057104</Text>
                        </View>
                    }


                </View>
                : <View style={{ borderRadius: 8, backgroundColor: 'white', flexDirection: 'row', flex: 1, justifyContent: 'space-around', alignItems: 'flex-end' }}>
                    {
                        loadingArray.map(x => {
                            return (
                                <View>

                                    <View
                                        style={{
                                            height: 10, width: 100,
                                            backgroundColor: '#d3d3d3', margin: 5,
                                        }}
                                    >
                                    </View>
                                    <View style={{ margin: 2 }} >
                                        <View
                                            style={{
                                                height: 55, width: 100,
                                                backgroundColor: '#d3d3d3'
                                            }}
                                        >

                                        </View>


                                    </View>

                                    <View style={{ margin: 5 }}>
                                        <View
                                            style={{ height: 15, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                        >

                                        </View>

                                    </View>
                                </View>
                            )

                        })}
                </View>

        )
    }

    render() {
        const {
            apps,
            latitude,
            longitude,
            transportList,
            smallCabQuote,
            mediumCabQuote,
            largeCabQuote,
            showLoadingCard,

            loadingArray,
            showCabCard,
            showDriverCard,
            isLocationPermissionGranted,
            isSideBarDisplayed
        } = this.state;
        return (
            <View style={styles.body}>

                {/* <GooglePlacesAutocomplete
                    placeholder='Search Destination'
                    onPress={(data, details = null) => {
                        // 'details' is provided when fetchDetails = true
                        console.log(data, details);
                    }}
                     currentLocation={true}
                     
                    query={{
                        key: 'AIzaSyC5OsgIe-JfB6yGD9Qa4J9yiCsQs6g9QIE',
                        language: 'en',
                    }}
                    style={{ backgroundColor: 'red', marginBottom: 0 }}
                /> */}
                {/* <View style={{ height: '10%' }}>
                    <TextInput style={{ borderWidth: 2, borderColor: 'black' }}
                        placeholder="Enter Destinatiion"
                    >
                    </TextInput>
                </View > */}
                <View style={{ marginHorizontal: 5, position: 'absolute', top: 10, zIndex: 10 }}>
                    <TouchableOpacity
                        style={{ width: 35 }}
                        onPress={() => this.setState({ isSideBarDisplayed: !isSideBarDisplayed })}>
                        <Image
                            style={{ marginVertical: 5, marginRight: 50, width: 35, height: 29, borderColor: 'white', tintColor: 'black' }}
                            source={{ uri: 'https://kaamkaajbackup.s3.ap-south-1.amazonaws.com/SidebarNavigationIcon.png' }} />
                    </TouchableOpacity>

                </View>
                {isSideBarDisplayed === true && <SideBar {...this.props} />}

                <View
                    onTouchEnd={() => { this.setState({ isSideBarDisplayed: false }) }}
                >
                    <View style={{ height: screenHeight * 0.50, width: screenWidth }} >
                        {(isLocationPermissionGranted === true && longitude && latitude) &&
                            (<MapView
                                style={styles.mapStyle}
                                showsUserLocation={true}
                                zoomEnabled={true}
                                zoomControlEnabled={true}
                                on
                                onRegionChange={this.onRegionChange}
                                initialRegion={{
                                    latitude,
                                    longitude,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}>

                                <Marker
                                    coordinate={{ latitude, longitude }}
                                    title={"MY Location"}
                                    description={"Vasudev krishna"}
                                />
                            </MapView>)


                        }
                    </View>
                    <View style={{ marginTop: 5, height: screenHeight * 0.40, width: screenWidth }} >
                        {
                            showCabCard && this.renderCabsCard()
                        }
                        {
                            showDriverCard && this.renderDriverCard()
                        }


                    </View>
                </View>

                {/* <Footer/> */}
            </View>
        )
    }
}


const mapStatetoProps = state => {
    // console.log('value from store is ', state)
    return {
        state
    }
}

export default connect(mapStatetoProps, null)(Transport);


const styles = StyleSheet.create({
    MainContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    mapStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },

    body: {
        height: screenHeight,
        width: screenWidth,
        flex: 1,
        backgroundColor: 'pink'

    },
    label: {
        fontSize: 16,
        // fontWeight:'bold'
    },

    createWorkBtn: {
        width: 120,
        height: 120,
        backgroundColor: '#09099B',
        borderColor: '#09099B',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    searchWorkBtn: {
        width: 120,
        height: 120,
        backgroundColor: '#AB0808',
        borderColor: '#AB0808',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'white',
        fontSize: 16,
        flexWrap: 'nowrap'
    }
});

import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
} from 'react-native';

import language from '../cms/lang/language'

export default class WorkOwnerDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#E4E0E0', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ backgroundColor: 'white', padding: 20, borderRadius: 30 }}>
                    <View style={{ marginTop: 15 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={styles.label}>Work Title</Text>
                            <Text style={styles.label}>Work Location</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={styles.label}>Work Description</Text>
                            <Text style={styles.label}>Work Owner</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 50, marginLeft: 35 }}>
                        <TouchableOpacity style={styles.submitBtn}>
                            <Text style={styles.text}>Call Owner</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    body: {

    },
    label: {
        fontSize: 24
    },
    textbox: {
        borderRadius: 8,
        borderWidth: 2,
        width: 338,
        height: 67,
        fontSize: 24
    },
    submitBtn: {
        width: 303,
        height: 47,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 120,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'white',
        fontSize: 24
    }
});

import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Picker,
    Modal,
    Image,
    TouchableWithoutFeedback,
    ProgressBarAndroid,
    Dimensions,
    AsyncStorage,
    BackHandler
} from 'react-native';

import SideBar from '../../Component/SideBar';

import Header from '../../Component/Header';

import language from '../../cms/lang/language';

import { getGetShoppingItems, CreateUpdateShoppingOrder } from '../../Service/Kaamkaaj';

import { TouchableHighlight } from 'react-native-gesture-handler';

import { SliderBox } from "react-native-image-slider-box";
import { connect } from 'react-redux';
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen')



class Shopping extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ShoppingList: [],
            isSideBarDisplayed: false,
            selectedItem: [],
            selectedValue: 1,
            showItemNumber: false,
            showSearchingCard: false,
            showBookedWorkerCard: false,
            userTotalPrice: 0,
            showSubmitProgressBar: false,
            userShoppingList: [],
            num: [],
            createShoppingrequest: {
                userShoppingOrders: {
                    OrderId: null,
                    TotalPrice: "900",
                    UserId: "12345",
                    IsActive: "1",
                    CreatedOn: null,
                    UpdatedOn: null,
                    PaymentStatus: "1",
                    DeliveryStatus: "2",
                    CustomerRemarks: "",
                    AgentRemarks: "",
                    Address: "fkjds",
                    UserMobileNo: "kkkk",
                    Items: "jhbjkjnkj"
                },
                ShoppingOrderItems: [
                    {
                        Id: null,
                        OrderNo: "fsdfsd",
                        MobileNo: "45654",
                        UserId: "456754",
                        ItemId: "r456765",
                        ItemName: "45676",
                        ItemUnit: "4567",
                        ItemPrice: "4567",
                        ItemQuantity: "9876",
                        IsActive: "1",
                        CreatedOn: "djlf",
                        Zorder: "2345"
                    }
                ]
            },
            loadingArray: [1, 2, 3, 4, 5, 6, 7]

        }
    }

    backAction = () => {

        this.props.navigation.navigate('Default')
    };

    async componentDidMount() {
        // this.backHandler = BackHandler.addEventListener(
        //     "hardwareBackPress",
        //     this.backAction
        // );

        const result = await getGetShoppingItems();


        result.shoppingItems.map(x => {
            x.quantity.sort((x, y) => {
                return x - y
            })

            x.itemQuantity = Math.min(...x.quantity)
        })
        if (result) {
            this.setState({
                ShoppingList: result.shoppingItems
            })
        }


    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backAction());
    }



    handelMenuDisplayer = () => {
        const {
            isSideBarDisplayed
        } = this.state;
        this.setState({ isSideBarDisplayed: !isSideBarDisplayed })
    }

    handelItemQuantity = (x) => {
        const {
            selectedItem,

            ShoppingList
        } = this.state;
        const index = ShoppingList.findIndex(x => x.id === selectedItem);
        if (index !== -1) {
            const price = ShoppingList[index].price / ShoppingList[index].quantity;
            ShoppingList[index].price = price * x;
            ShoppingList[index].quantity = x
        }
        this.setState({ showItemNumber: false, ShoppingList })
    }

    setQuantity = (quantity, id) => {
        const {
            ShoppingList
        } = this.state;
        const itemIndex = ShoppingList.findIndex(x => x.id === id);
        if (itemIndex !== -1) {
            ShoppingList[itemIndex].itemQuantity = quantity;
            ShoppingList[itemIndex].totalPrice = ShoppingList[itemIndex].price * quantity

        }
        this.setState(ShoppingList);

    }

    addItem = async (id, itemQuantity, unit, price, name) => {

        let {
            userShoppingList,
            userTotalPrice
        } = this.state;

        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);
        }
        if (userShoppingList && userShoppingList.length > 0) {
            const index = userShoppingList.findIndex(x => x.itemId === id)
            if (index !== -1) {
                userShoppingList[index].itemQuantity = userShoppingList[index].itemQuantity + itemQuantity;
                userShoppingList[index].itemPrice = userShoppingList[index].itemPrice + (itemQuantity * price);
            }
            else {
                userShoppingList.push({ id: 0, orderNo: 0, mobileNo: user.mobileNumber, userId: user.id, itemId: id, itemName: name, itemUnit: unit, itemPrice: itemQuantity * price, itemQuantity, isActive: 1, zorder: 1 })

            }
        }
        else {
            userShoppingList.push({ id: 0, orderNo: 0, mobileNo: user.mobileNumber, userId: user.id, itemId: id, itemName: name, itemUnit: unit, itemPrice: itemQuantity * price, itemQuantity, isActive: 1, zorder: 1 })

        }



        userTotalPrice += itemQuantity * price;

        this.setState({ userShoppingList, userTotalPrice });

    }
    removeSelectedItem = (id, quantity, totalPrice) => {
        let {
            userTotalPrice,
            userShoppingList
        } = this.state;
        const index = userShoppingList.findIndex(x => x.itemId === id)
        if (index !== -1) {
            userTotalPrice -= totalPrice;
            userShoppingList.splice(index, 1)
        }
        this.setState({ userShoppingList, userTotalPrice })

    }



    orderNow = async () => {
        const {
            userShoppingList,
            userTotalPrice,
            showSubmitProgressBar,
            createShoppingrequest
        } = this.state;
        this.setState({ showSubmitProgressBar: true })

        let user = await AsyncStorage.getItem('userLocalStorage', null)
        if (user) {
            user = JSON.parse(user);

            let itemsList = "";
            userShoppingList.map(x => {
                itemsList += `${x.itemName} ${x.itemQuantity} ${x.itemUnit} \u20B9${x.itemPrice} ,`
            })
            createShoppingrequest.ShoppingOrderItems = userShoppingList

            createShoppingrequest.userShoppingOrders =
                { OrderId: 0, TotalPrice: userTotalPrice, UserId: user.id, IsActive: 1, PaymentStatus: 1, DeliveryStatus: 1, CustomerRemarks: "", AgentRemarks: "", Address: `${user.address.address1} ${user.address.address2} ${user.address.city} ${user.address.pincode} `, UserMobileNo: user.mobileNumber, Items: itemsList }
           
        }

        let response = await CreateUpdateShoppingOrder(createShoppingrequest);
        if (response) {

            this.setState({ showSubmitProgressBar: false })
            let user = await AsyncStorage.getItem("userLocalStorage", null)

            if (user) {
                user = JSON.parse(user);
            }
            this.props.navigation.navigate('BookingConfirmation', { image: "image0", appType: "1", price: userTotalPrice, items: response.shoppingOrderItems, orderNo: response.userShoppingOrders.orderId, user })
        }
        else {
            this.setState({ showSubmitProgressBar: false })
        }


    }

    render() {
        const {
            showSubmitProgressBar,
            ShoppingList,
            isSideBarDisplayed,
            showItemNumber,
            num,
            showSearchingCard,
            showBookedWorkerCard,
            userShoppingList,
            userTotalPrice,
            loadingArray

        } = this.state;

        return (
            <SafeAreaView>
                <Header
                    handelMenuDisplayer={this.handelMenuDisplayer}
                />
                {isSideBarDisplayed === true && <SideBar {...this.props} />}
                <View style={{ marginTop: 20, display: `${userShoppingList.length > 0 ? 'flex' : 'none'}` }}>
                    <Text>ldkfjslkd fjsldkjfsldkjfsld fjsldkfjsdlkfj</Text>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10 }}>
                        {userShoppingList.map(x => {
                            const {
                                itemName,
                                itemPrice,
                                itemQuantity,
                                itemUnit,
                                itemId

                            } = x;
                            return (

                                <View style={{ flexDirection: 'row', position: 'relative' }}>
                                    <View style={{ backgroundColor: 'green', padding: 10, margin: 3 }}>
                                        <Text style={{ color: 'white', fontSize: 14 }}>{`${itemName} ${itemQuantity}${itemUnit} - \u20B9 ${itemPrice}`}</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={{ position: 'absolute', left: 0, top: 10, width: 20, height: 20, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}
                                        onPress={() => this.removeSelectedItem(itemId, itemQuantity, itemPrice)}
                                    >
                                        <Text style={{ color: 'white', fontSize: 16 }}>X</Text>
                                    </TouchableOpacity>

                                </View>)
                        })
                        }

                    </View>
                    <View style={{ alignSelf: 'flex-end' }}>
                        <TouchableOpacity
                            style={{ flexDirection: 'row', alignSelf: 'center', backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center', padding: 10, borderRadius: 8 }}
                            onPress={() => {
                                this.orderNow()
                            }}

                        >
                            <Text style={{ marginRight: 5, color: 'white' }}>{'\u20B9'} {userTotalPrice} ORDER NOW</Text>
                            <ProgressBarAndroid style={{ display: `${showSubmitProgressBar ? 'flex' : 'none'}`, height: 30, width: 30 }} color="#ffffff" />

                        </TouchableOpacity>
                    </View>
                </View>

                <ScrollView style={styles.body}>
                    <TouchableWithoutFeedback onPress={() => { this.setState({ isSideBarDisplayed: false }) }}>
                        <View style={{ flex: 1 }}>

                            <View style={{ backgroundColor: '#84C2F9', flex: 1, flexDirection: 'column' }}>


                                <View style={{ marginTop: 30, marginHorizontal: 5 }}>
                                    {
                                        ShoppingList && ShoppingList.length > 0 ? ShoppingList.map((x, index) => {
                                            const {
                                                name,
                                                id,
                                                description,
                                                description1,
                                                price,
                                                quantity,
                                                itemQuantity,
                                                photo,
                                                photo1,
                                                photo2,
                                                unit,
                                                createdOn,
                                                zOrder,
                                                type,
                                                isActive
                                            } = x;
                                            return (
                                                <View
                                                    style={[styles.chips]}
                                                >
                                                    <View style={{ width: 130, marginHorizontal: 5 }}>

                                                        <SliderBox
                                                            images={[photo]}
                                                            onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                                                            sliderBoxHeight={80}
                                                            parentWidth={130}

                                                            dotColor="#FFEE58"
                                                            inactiveDotColor="white"
                                                            autoplay

                                                            dotStyle={{
                                                                width: 15,
                                                                height: 15,
                                                                borderRadius: 15,
                                                                marginHorizontal: 10,
                                                                padding: 0,
                                                                margin: 0
                                                            }}

                                                        />
                                                    </View>


                                                    <View style={{ width: `30%` }}>
                                                        <Text style={{ color: 'black', fontSize: 16 }}>
                                                            {/* {language.hindi.workMaster.filter(x => x.id == id)[0].value} */}
                                                            {name}
                                                        </Text>
                                                        <View style={{ flexDirection: 'row' }}>

                                                            <Picker
                                                                selectedValue={itemQuantity}
                                                                style={{ borderWidth: 2, borderColor: 'black', height: 40, width: 75, borderStartWidth: 3 }}
                                                                onValueChange={(itemValue, itemIndex) => this.setQuantity(itemValue, id)}
                                                                mode='dropdown'
                                                            >
                                                                {
                                                                    quantity.map(x => {
                                                                        return (<Picker.Item label={`${x}`} value={x} />)
                                                                    })
                                                                }
                                                            </Picker>

                                                        </View>
                                                    </View>


                                                    <View style={{ width: "30%", flexDirection: 'column' }}>
                                                        <Text style={{ color: 'black', fontSize: 16 }}>{`\u20B9${price}/${unit}`}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => { this.addItem(id, itemQuantity, unit, price, name) }}
                                                            style={{ marginVertical: 10, height: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0A1AF5' }}>
                                                            <Text style={{ color: 'white', fontSize: 16 }}>Add</Text>
                                                        </TouchableOpacity>

                                                    </View>

                                                </View>
                                            )
                                        }) : loadingArray.map(x => {
                                            return (
                                                <View style={{
                                                    backgroundColor: 'white',
                                                    width: screenWidth,
                                                    height: 90,
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',
                                                    borderRadius: 8,
                                                    marginVertical: 5



                                                }}>

                                                    <View
                                                        style={{
                                                            height: 80, width: 150,
                                                            backgroundColor: '#d3d3d3', margin: 5,

                                                        }}
                                                    >

                                                    </View>

                                                    <View style={{ justifyContent: 'space-around' }} >
                                                        <View
                                                            style={{
                                                                height: 15, width: 100,

                                                                backgroundColor: '#d3d3d3'
                                                            }}
                                                        >

                                                        </View>
                                                        <View
                                                            style={{ height: 15, width: 100, backgroundColor: '#d3d3d3' }}
                                                        >

                                                        </View>

                                                    </View>

                                                    <View style={{ justifyContent: 'space-around' }}>
                                                        <View
                                                            style={{ height: 15, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                                        >

                                                        </View>
                                                        <View
                                                            style={{ height: 50, width: 100, backgroundColor: '#d3d3d3', marginRight: 5 }}
                                                        >

                                                        </View>
                                                    </View>
                                                </View>
                                            )

                                        })





                                    }
                                </View>
                            </View>

                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </SafeAreaView>
        )
    }
}


const mapStatetoProps = state => {
    return {
        state
    }
}

const mapDispatchtoProps = dispatch => {
    return {
        saveUserData: data => dispatch(saveUserData(data))
    };
};

export default connect(mapStatetoProps, mapDispatchtoProps)(Shopping);

const styles = StyleSheet.create({

    body: {
        marginTop: 40,

    },
    chips: {

        // borderWidth: 1,
        // borderColor: '#f0f0f0',
        borderRadius: 8,
        backgroundColor: 'white',
        // width: 195,
        height: 90,
        marginVertical: 5,

        flexDirection: 'row'

    },
    example: {
        marginVertical: 0,
    },
    activeChips: {
        backgroundColor: 'green'
    },
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },
    card: {

        // backgroundColor: 'white',
        // borderRadius: 8,
        // height: 300,
        // marginBottom: 15,
        // overflow: 'scroll',


    },
    label: {
        fontSize: 16
    },
    textbox: {
        borderRadius: 18,
        borderWidth: 1,
        width: '100%',
        height: 50,
        fontSize: 16
    },
    submitBtn: {
        width: 164,
        height: 46,
        backgroundColor: '#18B155',
        borderColor: '#18B155',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    text: {
        color: 'red',
        fontSize: 16
    }
});
